(function (__dir) {

    const _fs = require("fs");

    const _interf = require(__dir("factory", "interf"));
    const _depot = require(__dir("factory", "depot"));

    (function () {

        module.exports = _componentViewMenu;

    }());

    function _componentViewMenu() {

        return _procedure;

        function _procedure(_resolve) {
            const _context = {
                    data: function () {
                            return _depot.appInstance.$data;
                        },
                    template: _fs.readFileSync("./template/menu.html", _depot.CHAR_SET),
                    beforeCreate: _onCreated
                };
            _resolve(_context);
        }

    }

    function _onCreated() {

        var _response = _interf.fileSync(__dir("json", "hypercategory.json"));
        _depot.appInstance.hypercategoryList = _response.hypercategory;

    }

}(global.__dir));