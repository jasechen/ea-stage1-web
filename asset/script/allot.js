(function () {

    "use strict";

    var _allotDictionary;

    (function () {

        _allotDictionary = {};
        window.allot = window.allot || _allotter ;

    }());

    function _allotter(_key, _source) {

        if ( _source ) _allotDictionary[_key] = _source;
        return _allotDictionary[_key];

    }

}());