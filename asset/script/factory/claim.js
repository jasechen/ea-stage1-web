(function (_allot) {

    "use strict";

    (function () {

        _allot("claim", _claim);

    }());

    function _claim(_method, _url, _data, _callback, _errorback, _finishback) {

        _method = _method.toUpperCase();
        _url = _getInspectedUrl.apply(null, arguments);
        _data = _getInspectedData.apply(null, arguments);
        _process(_method, _url, _data, _callback, _errorback, _finishback);

    }

    function _getInspectedUrl(_method, _url) {

        _url = _getQueriedUrl.apply(null, arguments);
        if ( _method === "GET" ) _url += "?v=" + ( +new Date() );
        if ( !/\.\w+/.test(_url) ) _url = _allot("path").API + _url ;
        return _url;

    }

    function _getInspectedData(_method, _url, _data) {

        return _stringifyData(_data);

    }

    function _process(_method, _url, _data, _callback, _errorback, _finishback) {

        var _xhttp = new XMLHttpRequest();
        _xhttp.onreadystatechange = _onReadyStateChange;
        _xhttp.open(_method, _url, true);
        if ( !/\.\w+$/.test(_url) ) {
            if ( _allot("session") ) _xhttp.setRequestHeader("Session", _allot("session"));
            if ( _allot("token") ) _xhttp.setRequestHeader("Token", _allot("token"));
        }
        if ( typeof(_data) === "string" ) _xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=UTF-8");
        _xhttp.send(_data);

        function _onReadyStateChange() {
            if ( _xhttp.readyState !== 4 ) return;
            var _response = _xhttp.responseText;
            if ( /20\d/.test(_xhttp.status) ) {
                if ( !/\.html($|\??)/.test(_url) && ( _xhttp.status === 200 || _xhttp.status === 201 ) ) _response = _getParsed(_response);
                if ( _response.code ) _response = _response.data;
                if ( _callback ) _callback(_response);
            } else if ( _errorback ) _errorback(_getParsed(_response));
            if ( _finishback ) _finishback();
        }

        function _getParsed(_response) {
            return JSON.parse( _response || null ) || {} ;
        }

    }

    function _getQueriedUrl(_method, _url, _data) {

        var _queries = _url.match(/\{\w+\}/g);
        var _item;
        if ( _queries ) {
            while ( _queries.length ) {
                _item = _queries.shift();
                _url = _url.replace(_item, _data[_item]);
                delete _data[_item];
            }
        }
        return _url;

    }

    function _stringifyData(_data) {

        var _isFileForm = _conformDataFile(_data);
        var _requestData = ( _isFileForm ) ? new FormData() : "";
        var _key;
        for ( _key in _data ) _onForEach(_key, _data[_key]);
        return _requestData;

        function _onForEach(_key, _val) {
            if ( _isFileForm ) _requestData.append(_key, _val);
            else _requestData += ( _requestData.length ? "&" : "" ) + _confirmedValue(_key, _val);
        }

        function _conformDataFile(_data) {
            var _nowKey;
            for ( _nowKey in _data ) {
                if ( _data[_nowKey] instanceof(Blob) ) return true;
                if ( _data[_nowKey] instanceof(File) ) return true;
            }
            return false;
        }

        function _confirmedValue(_keyName, _val) {
            if ( ( _val === null ) || ( typeof(_val) !== "object" ) ) {
                _val = String( _val || "" ).replace(/\&/g, "%26");
                return _keyName + "=" + _val ;
            }
            return _collectedDatum(_val, _keyName);
        }

        function _collectedDatum(_collect, _parentName) {
            var _result = "";
            var _isFirst = true;
            var _key;
            var _val;
            _parentName = _parentName || "";
            for ( _key in _collect ) {
                _val = _collect[_key];
                _result += ( _isFirst ? "" : "&" ) + _confirmedValue( _parentName + "%5B" + _key + "%5D" , _val);
                if ( _isFirst ) _isFirst = null;
            }
            return _result;
        }

    }

}(window.allot));