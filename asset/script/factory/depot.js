(function (_allot) {

    "use strict";

    var _depotInstance;

    (function () {

        _depotInstance = {};
        _depotInstance.FALLSVIEW_CONTAINER_SELECTOR = "section.fallsview .container";
        _depotInstance.FALLSVIEW_ITEM_SUB_SELECTOR = ".card";
        _depotInstance.FALLSVIEW_ITEM_SELECTOR = _depotInstance.FALLSVIEW_CONTAINER_SELECTOR + " " + _depotInstance.FALLSVIEW_ITEM_SUB_SELECTOR ;
        _allot("depot", _depotInstance);

    }());

}(window.allot));