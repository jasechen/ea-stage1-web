(function (_allot) {

    "use strict";

    var __cookie = _allot("cookie");
    var __claim = _allot("claim");

    var _lifesession;
    var _lifetoken;

    (function () {

        _allot("life", {
                session: _procedureSession,
                token: _procedureToken
            });

    }());

    function _procedureSession() {

        _lifesession = __cookie.LIFESESSION;
        _allot("session", _lifesession);
        if ( _lifesession ) _inspectSession();
        else _initSession();

    }

    function _procedureToken(_callback, _errorback) {

        _lifetoken = __cookie.LIFETOKEN;
        _inspectToken(_callback, _errorback);

    }

    function _inspectSession() {

        __claim("GET", "session/{session_code}", {
                "{session_code}": _lifesession
            }, _onSessionSuccess, _onSessionError);

        function _onSessionSuccess(_response) {
            if ( _response.status === "expire" ) _initSession();
        }

        function _onSessionError() {
            _initSession();
        }

    }

    function _initSession() {

        __claim("POST", "session/init", null, _onSessionInitSuccess);

        function _onSessionInitSuccess(_response) {
            var _session = _response.session;
            _allot("session", _session);
            __cookie.addItem("LIFESESSION", _session);
            window.location.reload();
        }

    }

    function _inspectToken(_callback, _errorback) {

        if ( _lifetoken ) __claim("GET", "user/token/{token}", {
                "{token}": _lifetoken
            }, _onTokenSuccess, _onTokenError);
        else _onTokenError();

        function _onTokenSuccess(_response) {
            var _responseUser = _response.user;
            if ( !(_responseUser) ) return _onTokenError();
            _responseUser.profile = _responseUser.profile || {} ;
            _responseUser.profile.phone = _getInspectPhone(_responseUser.profile.mobile_phone);
            _allot("token", _lifetoken);
            if ( _callback ) _callback(_responseUser);
        }

        function _onTokenError() {
            __cookie.removeItem("LIFETOKEN");
            if ( _errorback ) _errorback();
        }

    }

    function _getInspectPhone(_phone) {

        if ( _phone.slice(0, 1) === "0" ) return _phone;
        return "0" + _phone ;

    }

}(window.allot));