(function (_allot, Masonry) {

    "use strict";

    var __depot = _allot("depot");

    var _masonry;

    (function () {

        _allot("masonry", {
                reset: _resetInstance,
                create: _createInstance,
                update: _updateInstance,
                delete: _removeInstance,
                append: _appendInstance,
                appendAll: _consignAppendAll,
                removeAll: _consignRemoveAll,
                addEvent: _addEventInstance
            });

    }());

    function _resetInstance() {

        if ( document.querySelector(__depot.FALLSVIEW_ITEM_SUB_SELECTOR) ) _createInstance();

    }

    function _createInstance() {

        _masonry = new Masonry(__depot.FALLSVIEW_CONTAINER_SELECTOR, {
                itemSelector: __depot.FALLSVIEW_ITEM_SELECTOR,
                columnWidth: __depot.FALLSVIEW_ITEM_SELECTOR,
                gutter: 12,
                transitionDuration: 0,
                fitWidth: true
            });

    }

    function _updateInstance() {

        if ( _masonry ) _masonry.layout();

    }

    function _removeInstance() {

        _masonry = null;

    }

    function _appendInstance(_element) {

        if ( _masonry ) _masonry.appended(_element);

    }

    function _consignAppendAll() {

        if ( !(_masonry) ) return;
        var _itemElements = document.querySelectorAll(__depot.FALLSVIEW_ITEM_SUB_SELECTOR);
        if ( _itemElements.length ) _masonry.appended(_itemElements);

    }

    function _consignRemoveAll() {

        if ( !(_masonry) ) return;
        var _itemElements = document.querySelectorAll(__depot.FALLSVIEW_ITEM_SUB_SELECTOR);
        if ( _itemElements.length ) _masonry.remove(_itemElements);

    }

    function _addEventInstance(_callback) {

        if ( _masonry ) _masonry.on("layoutComplete", _onEvent);

        function _onEvent() {
            if ( _callback ) _callback();
        }

    }

}(window.allot, window.Masonry));