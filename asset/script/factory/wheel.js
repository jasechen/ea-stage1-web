(function (_allot) {

    "use strict";

    var _callbackList = [];
    var _offset = {};

    (function () {

        _defineOffset();
        window.addEventListener("scroll", _onWheel);
        _allot("wheel", {
                add: _addCallback
            });

    }());

    function _defineOffset() {

        _callbackList = [];
        _offset = {};
        _offset.top = _offset.center = _offset.bottom = 0;

    }

    function _onWheel() {

        var _windowHeight = window.innerHeight;
        _offset.top = window.scrollY;
        _offset.center = parseInt( _offset.top + _windowHeight * 0.5 );
        _offset.bottom = _offset.top + _windowHeight ;
        _consignCallback();

    }

    function _addCallback(_callback) {

        if ( typeof(_callback) !== "function" ) return;
        _callbackList.push(_callback);

    }

    function _consignCallback() {

        var _index = _callbackList.length;
        var _callback;
        while ( _index-- ) {
            _callback = _callbackList[_index];
            if ( typeof(_callback) === "function" ) _callback(_offset);
        }

    }

}(window.allot));