(function (_allot) {

    "use strict";

    var _pathConfig;
    var _envArray;
    var _env;

    (function () {

        _allot("path", {});
        _defineVariable();
        _setPathProperties();

    }());

    function _defineVariable() {

        _envArray = [
                "RELEASE",
                "STAGING",
                "DEVELOP"
            ];
        _pathConfig = {
                RELEASE_HOST_PATH: "//app.english.agency/",
                RELEASE_API_PATH: "//apiapp.english.agency/",
                RELEASE_GALLERY_PATH: "//app.english.agency/",
                STAGING_HOST_PATH: "//s1.ea.psycomputing.com",
                STAGING_API_PATH: "//api.s1.ea.psycomputing.com/",
                STAGING_GALLERY_PATH: "//s1.ea.psycomputing.com/",
                DEVELOP_HOST_PATH: "//127.0.0.1:3001/",
                DEVELOP_API_PATH: "//dev.api.v1.english.agency/",
                DEVELOP_GALLERY_PATH: "//dev.v1.english.agency/"
            };

    }

    function _setPathProperties() {

        _env = _getInspectedEnv();
        _allot("path").ENV = window.env = _env;
        _allot("path").HOST = _pathConfig[ _env + "_HOST_PATH" ] || "" ;
        _allot("path").API = _pathConfig[ _env + "_API_PATH" ] || "" ;
        _allot("path").GALLERY = _pathConfig[ _env + "_GALLERY_PATH" ] || "" ;

    }

    function _getInspectedEnv() {

        var _index = _envArray.length;
        var _item;
        while ( _index-- > 0 ) {
            _item = _envArray[_index];
            if ( window.location.href.indexOf(_pathConfig[ _item + "_HOST_PATH" ]) < 0 ) continue;
            return _item;
        }

    }

}(window.allot));