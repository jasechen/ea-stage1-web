(function (_allot) {

    "use strict";

    var __app = _allot("app");
    var _methods = {};

    (function () {

        __app.$popup = {
                login: _sendLogin,
                register: _sendRegister,
                prompt: _sendPrompt,
                confirm: _sendConfirm,
                picture: _sendPicture,
                alert: _sendAlert,
                loading: _sendLoading,
                close: _sendClose,
                methods: _methods
            };

    }());

    function _sendLogin(_confirmback) {

        _methods.popupLoginConfirm = _onConfirm;
        _process("login");

        function _onConfirm() {
            if ( _confirmback ) _confirmback();
        }

    }

    function _sendRegister(_confirmback) {

        _methods.popupRegisterConfirm = _onConfirm;
        _process("register");

        function _onConfirm() {
            if ( _confirmback ) _confirmback();
        }

    }

    function _sendPrompt(_message, _default, _confirmback) {

        __app.popupPromptMessage = _message;
        __app.popupPromptValue = _default || "" ;
        _methods.popupPromptConfirm = _onConfirm;
        _process("prompt");

        function _onConfirm() {
            if ( _confirmback ) _confirmback(__app.popupPromptValue);
        }

    }

    function _sendPicture(_url, _confirmback) {

        __app.popupPictureURL = _url;
        _methods.popupPictureConfirm = _onConfirm;
        _process("picture");

        function _onConfirm() {
            if ( _confirmback ) _confirmback();
        }

    }
    function _sendConfirm(_message, _confirmback) {

        __app.popupConfirmMessage = _message;
        _methods.popupConfirmConfirm = _onConfirm;
        _process("confirm");

        function _onConfirm() {
            if ( _confirmback ) _confirmback();
        }

    }

    function _sendAlert(_message, _confirmback) {

        __app.popupAlertMessage = _message;
        _methods.popupAlertConfirm = _onConfirm;
        _process("alert");

        function _onConfirm() {
            if ( _confirmback ) _confirmback();
            else _sendClose("alert");
        }

    }

    function _sendLoading() {

        _process("loading");

    }

    function _sendClose(_statName) {

        if ( _statName ) {
            var _index = __app.popupstats.indexOf(_statName);
            if ( _index < 0 ) return;
            __app.popupstats.splice(_index, 1);
        } else __app.popupstats.shift();

    }

    function _process(_statName) {

        _setStats(_statName);

    }

    function _setStats(_statName) {

        if ( __app.popupstats.indexOf(_statName) < 0 ) __app.popupstats.unshift(_statName);

    }

}(window.allot));