(function (_allot) {

    "use strict";

    var __query = _allot("query");
    var __claim = _allot("claim");
    var __app = _allot("app");

    (function () {

        _allot("cmsdormcreate-directive", _componentDirective);
        _allot("cmsdormcreate-directive-mixin", _definedData());

    }());

    function _componentDirective(_resolve) {

        __app = _allot("app");
        __claim("GET", "/template/cmssection.dormcreate.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                inherit: true,
                    data: function () {
                            __app.$data.ROOM_TYPE_DICTIONARY = [
                                    "single",
                                    "double",
                                    "triple",
                                    "quadruple",
                                    "five-people",
                                    "six-people",
                                    "eight-people",
                                    "semi-double",
                                    "twin",
                                    "suit-2",
                                    "suit-3",
                                    "suit-4"
                                ];
                            return __app.$data;
                        },
                    template: _response,
                    beforeCreate: _beforeCreate,
                    methods: {
                            onCreateClick: _onCreateClick,
                            onRoomDeleteClick: _onRoomDeleteClick
                        },
                    watch: {
                            dormDefaultRoomType: _onDefaultRoomTypeChange,
                        }
                });
        }

    }

    function _definedData() {

        return {
                data: _procedure
            };

        function _procedure() {
            return {
                    cmsSchoolID: "",
                    dormType: "",
                    dormTypeLocale: "",
                    dormService: "",
                    dormFacility: "",
                    dormRoomList: [],
                    dormDefaultRoomType: ""
                };
        }

    }

    function _beforeCreate() {

        __app.cmsSchoolID = __app.cmsSchoolID || __query.all().school ;
        __app.dormType = __app.slug;
        __app.dormService = "";
        __app.dormFacility = "";
        __app.dormRoomList = [];
        __app.dormDefaultRoomType = "";

    }

    function _onCreateClick() {

        __app.$popup.loading();
        __claim("POST", "school/dorm", {
                school_id: __app.cmsSchoolID,
                type: __app.dormType,
                service: __app.dormService,
                facility: __app.dormFacility,
                rooms: __app.dormRoomList
            }, _onSuccess, _onError, _onFinish);

        function _onSuccess() {
            __app.$popup.alert("新增成功", _confirmSuccess);
        }

        function _confirmSuccess() {
            __app.$popup.close("alert");
            window.location.hash = "#/school/" + __app.cmsSchoolID ;
        }

        function _onError() {
            __app.$popup.alert("資料有誤");
        }

        function _onFinish() {
            __app.$popup.close("loading");
        }

    }

    function _onRoomDeleteClick(_index) {

        __app.dormRoomList.splice(_index, 1);

    }

    function _onDefaultRoomTypeChange() {

        if ( !(__app.dormDefaultRoomType) ) return;
        __app.dormRoomList.push({
                type: __app.dormDefaultRoomType
            });
        __app.dormDefaultRoomType = "";

    }

}(window.allot));