(function (_allot) {

    "use strict";

    var __claim = _allot("claim");
    var __app = _allot("app");

    (function () {

        _allot("cmsprofile-directive", _componentDirective);
        _allot("cmsprofile-directive-mixin", _definedData());

    }());

    function _componentDirective(_resolve) {

        __app = _allot("app");
        __claim("GET", "/template/cmssection.profile.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                    data: function () {
                            return __app.$data;
                        },
                    template: _response,
                    mounted: _mounted,
                    methods: {
                            onProfileModifyClick: _onProfileModifyClick,
                            onProfileReformClick: _onProfileReformClick,
                            onProfileEmailVerify: _onProfileEmailVerify,
                            onProfileEmailSend: _onProfileEmailSend,
                            onProfilePhoneVerify: _onProfilePhoneVerify,
                            onProfilePhoneSend: _onProfilePhoneSend
                        }
                });
        }

    }

    function _definedData() {

        return {
                data: _procedure
            };

        function _procedure() {
            return {
                };
        }

    }

    function _mounted() {



    }

    function _onProfileModifyClick() {

        _changeProfileModifyStat();

    }

    function _onProfileReformClick() {

        if ( _confirmFormEmpty() ) return _onError();
        __claim("put", "user/{id}/profile", {
                    "{id}": __app.tokenUser.id,
                    "first_name": __app.tokenUser.firstName,
                    "last_name": __app.tokenUser.lastName,
                    "nickname": __app.tokenUser.nickname,
                    "bio": __app.tokenUser.bio
                }, _onSuccess, _onError);

        function _onSuccess() {
            __app.$popup.alert("更新成功", _onConfirm);
        }

        function _onError(_response) {
            _response = _response || {} ;
            if ( _response.comment === "updateData empty" ) _changeProfileModifyStat();
            else __app.$popup.alert("更新有誤");
        }

        function _onConfirm() {
            _changeProfileModifyStat();
            __app.$popup.close("alert");
        }

        function _confirmFormEmpty() {
            if ( !(__app.tokenUser.firstName) ) return true;
            if ( !(__app.tokenUser.lastName) ) return true;
        }

    }

    function _onProfileEmailVerify() {

        __app.$popup.prompt("輸入信箱驗證碼", null, _onConfirm);

        function _onConfirm(_value) {
            __app.$popup.loading();
            __claim("put", "user/{id}/email/verify", {
                    "{id}": __app.tokenUser.id,
                    "check_code": __app.popupPromptValue
                }, _onSuccess, _onError, _onFinish);
        }

        function _onSuccess() {
            __app.$popup.alert("驗證成功", _onConfirmSuccess);
        }

        function _onError() {
            __app.popupPromptValue = "";
            __app.$popup.alert("驗證碼有誤");
        }

        function _onFinish() {
            __app.$popup.close("loading");
        }

        function _onConfirmSuccess() {
            window.location.reload();
        }

    }

    function _onProfileEmailSend() {

        __app.$popup.confirm("重發信箱驗證信？", _onConfirm);

        function _onConfirm() {
            __app.$popup.loading();
            __claim("get", "user/{id}/email/notify", {
                    "{id}": __app.tokenUser.id
                }, _onSuccess, _onError, _onFinish);
        }

        function _onSuccess() {
            __app.$popup.alert("發信成功", _onConfirmSuccess);
        }

        function _onError() {
            __app.popupPromptValue = "";
            __app.$popup.alert("發信有誤");
        }

        function _onFinish() {
            __app.$popup.close("loading");
        }

        function _onConfirmSuccess() {
            window.location.reload();
        }

    }

    function _onProfilePhoneVerify() {

        __app.$popup.prompt("輸入手機驗證碼", null, _onConfirm);

        function _onConfirm() {
            __app.$popup.loading();
            __claim("put", "user/{id}/mobile/verify", {
                    "{id}": __app.tokenUser.id,
                    "check_code": __app.popupPromptValue
                }, _onSuccess, _onError, _onFinish);
        }

        function _onSuccess() {
            __app.$popup.alert("驗證成功", _onConfirmSuccess);
        }

        function _onError() {
            __app.popupPromptValue = "";
            __app.$popup.alert("驗證碼有誤");
        }

        function _onFinish() {
            __app.$popup.close("loading");
        }

        function _onConfirmSuccess() {
            window.location.reload();
        }

    }

    function _onProfilePhoneSend() {

        __app.$popup.confirm("重發手機驗證信？", _onConfirm);

        function _onConfirm() {
            __app.$popup.loading();
            __claim("get", "user/{id}/mobile/notify", {
                    "{id}": __app.tokenUser.id
                }, _onSuccess, _onError, _onFinish);
        }

        function _onSuccess() {
            __app.$popup.alert("發信成功", _onConfirmSuccess);
        }

        function _onError() {
            __app.popupPromptValue = "";
            __app.$popup.alert("發信有誤");
        }

        function _onFinish() {
            __app.$popup.close("loading");
        }

        function _onConfirmSuccess() {
            window.location.reload();
        }

    }

    function _changeProfileModifyStat() {

        __app.cmsprofilemodify = !(__app.cmsprofilemodify);

    }

}(window.allot));