(function (_allot) {

    "use strict";

    var __claim = _allot("claim");
    var __app = _allot("app");

    (function () {

        _allot("cmsagentcreate-directive", _componentDirective);

    }());

    function _componentDirective(_resolve) {

        __app = _allot("app");
        __claim("GET", "/template/cmssection.agentcreate.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                    inherit: true,
                    data: function () {
                            return __app.$data;
                        },
                    template: _response,
                    beforeCreate: _beforeCreate,
                    mounted: _mounted,
                    methods: {
                            onCreateClick: _onCreateClick,
                            onAvatarChange: _onAvatarChange
                        }
                });
        }

    }

    function _beforeCreate() {

        _allot("cmsagent-controller").initDatum();

    }

    function _mounted() {

        _readCompany();
        _readFilterCountries();

    }

    function _readCompany() {

        __claim("get", "company/list", {
            }, _onSuccess, _onError);

        function _onSuccess(_response) {
            var _responseCompanies = _response.companies;
            __app.agentDefaultCompanyID = ( _responseCompanies[0] || {} ).id;
            if ( !(__app.agentDefaultCompanyID) ) _onError();
        }

        function _onError() {
            __app.$popup.alert("發生錯誤");
        }

    }

    function _readFilterCountries() {

        __claim("get", "filter/school/type/country", {
            }, _onSuccess, null, _onFinish);

        function _onSuccess(_response) {
            __app.targetCountryList = _response.filters || [] ;
        }

        function _onFinish() {
            _readFilterLocations();
        }

    }

    function _readFilterLocations() {

        __claim("get", "user/agent/profile/locations", {
            }, _onSuccess);

        function _onSuccess(_response) {
            __app.locationDictionary = _response.locations || {} ;
        }

    }

    function _onCreateClick() {

        _allot("cmsagent-controller").inspectSingle(_createAgent);

    }

    function _createAgent() {

        __app.$popup.loading();
        _allot("cmsagent-controller").createSingle(_onSuccess, _onError, _onFinish);

        function _onSuccess() {
            __app.$popup.alert("新增成功", _confirmSuccess);
        }

        function _onError() {
            __app.$popup.alert("資料有誤");
        }

        function _onFinish() {
            __app.$popup.close("loading");
        }

        function _confirmSuccess() {
            __app.$popup.close("alert");
            window.location.hash = "#/agent/" + __app.agentDatum.id ;
        }

    }

    function _onAvatarChange(_event) {

        var _file = _event.target.files[0];

        var _fileReader = new FileReader();
        _fileReader.onload = _onFileReadLoaded;
        _fileReader.readAsDataURL(_file);

        function _onFileReadLoaded() {
            __app.agentDatum.avatarURL = _fileReader.result;
        }

    }

}(window.allot));