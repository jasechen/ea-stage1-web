(function (_allot) {

    "use strict";

    var __claim = _allot("claim");
    
    
    (function () {

        _allot("cmsmenu-directive", _componentCMSMenuView);
        _allot("cmsmenu-directive-mixin", _definedData());

    }());

    function _componentCMSMenuView(_resolve) {

        __claim("GET", "/template/cmsmenu.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                    data: function () {
                            return _allot("app").$data;
                        },
                    template: _response,
                    methods: {
                        getMenuClass: _getMenuClass,
                        onCmsMobileMenu: _onCmsMobileMenu
                    }
                });
        }

    }

    function _definedData() {

        return {
                data: _procedure
            };

        function _procedure() {
            return {
                    visiableCmsMenuStat: false
                };
        }

    }
    
    function _getMenuClass() {

        return {
                "work": _allot("app").visiableCmsMenuStat
            };

    }
    
    function _onCmsMobileMenu(_response) {
        _allot("app").visiableCmsMenuStat = !(_allot("app").visiableCmsMenuStat);
    }
    
}(window.allot));