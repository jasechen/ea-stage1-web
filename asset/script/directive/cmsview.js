(function (_allot) {

    "use strict";

    var __claim = _allot("claim");
    var __app = _allot("app");

    (function () {

        _allot("cmsview-directive", _componentCMSView);
        _allot("cmsview-directive-mixin", _definedData());

    }());

    function _componentCMSView(_resolve) {

        __app = _allot("app");
        __claim("GET", "/template/cmsview.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                inherit: true,
                    data: function () {
                            return __app.$data;
                        },
                    template: _response,
                    components: {
                            "view-cmsprofile": _allot("cmsprofile-directive"),
                            "view-cmsschoollist": _allot("cmsschoollist-directive"),
                            "view-cmsschoolcreate": _allot("cmsschoolcreate-directive"),
                            "view-cmsschoolsingle": _allot("cmsschoolsingle-directive"),
                            "view-cmspicturelist": _allot("cmspicturelist-directive"),
                            "view-cmscourselist": _allot("cmscourselist-directive"),
                            "view-cmscoursecreate": _allot("cmscoursecreate-directive"),
                            "view-cmscoursesingle": _allot("cmscoursesingle-directive"),
                            "view-cmsdormlist": _allot("cmsdormlist-directive"),
                            "view-cmsdormcreate": _allot("cmsdormcreate-directive"),
                            "view-cmsdormsingle": _allot("cmsdormsingle-directive"),
                            "view-cmsagentlist": _allot("cmsagentlist-directive"),
                            "view-cmsagentsingle": _allot("cmsagentsingle-directive"),
                            "view-cmsagentcreate": _allot("cmsagentcreate-directive"),
                            "view-cmsagentbinding": _allot("cmsagentbinding-directive"),
                            "view-cmsskilllist": _allot("cmsskilllist-directive"),
                            "view-cmsskillsingle": _allot("cmsskillsingle-directive"),
                            "view-cmsskillcreate": _allot("cmsskillcreate-directive"),
                            "view-cmsrecommendlist": _allot("cmsrecommendlist-directive"),
                            "view-cmsrecommendsingle": _allot("cmsrecommendsingle-directive"),
                            "view-cmsrecommendcreate": _allot("cmsrecommendcreate-directive"),
                            "view-cmsessaylist": _allot("cmsessaylist-directive")
                        }
                });
        }

    }

    function _definedData() {

        return {
                data: _procedureData,
                methods: _procedureMethods()
            };

        function _procedureData() {
            return {
                    cmsmodify: false
                };
        }

        function _procedureMethods() {
            return {
                    onModifyClick: _onModifyClick,
                    onModifyCancel: _onModifyCancel
                };
        }

    }

    function _onModifyClick() {

        _changeModifyStat();

    }

    function _onModifyCancel() {

        window.location.reload();

    }

    function _changeModifyStat() {

        __app.cmsmodify = !(__app.cmsmodify);

    }

}(window.allot));