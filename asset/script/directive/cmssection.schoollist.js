(function (_allot) {

    "use strict";

    var __claim = _allot("claim");
    var __app = _allot("app");

    (function () {

        _allot("cmsschoollist-directive", _componentDirective);
        _allot("cmsschoollist-directive-mixin", _definedData());

    }());

    function _componentDirective(_resolve) {

        __app = _allot("app");
        __claim("GET", "/template/cmssection.schoollist.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                    data: function () {
                            return __app.$data;
                        },
                    template: _response,
                    mounted: _mounted,
                    methods: {
                        }
                });
        }

    }

    function _definedData() {

        return {
                data: _procedure
            };

        function _procedure() {
            return {
                    cmsSchoolList: []
                };
        }

    }

    function _mounted() {

        __app.$popup.loading();
        __claim("get", "school/list", {
            }, _onSuccess, null, _onFinish);

        function _onSuccess(_response) {
            __app.cmsSchoolList = _response.schools || [] ;
            __app.cmsSchoolList.reverse();
        }

        function _onFinish() {
            __app.$popup.close("loading");
        }

    }

}(window.allot));