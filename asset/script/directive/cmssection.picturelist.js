(function (_allot) {

    "use strict";

    var __claim = _allot("claim");
    var __app = _allot("app");

    var _fileInput;

    (function () {

        _allot("cmspicturelist-directive", _componentDirective);
        _allot("cmspicturelist-directive-mixin", _definedData());

    }());

    function _componentDirective(_resolve) {

        __app = _allot("app");
        __claim("GET", "/template/cmssection.picturelist.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                inherit: true,
                    data: function () {
                            return __app.$data;
                        },
                    template: _response,
                    beforeCreate: _beforeCreate,
                    mounted: _mounted,
                    computed: {
                            schoolPurePictureList: _getSchoolPurePictureList
                        },
                    methods: {
                            onCreateClick: _onCreateClick
                        }
                });
        }

    }

    function _definedData() {

        return {
                data: _procedure
            };

        function _procedure() {
            return {
                    schoolPictureList: []
                };
        }

    }

    function _beforeCreate() {



    }

    function _mounted() {



    }

    function _getSchoolPurePictureList() {

        var _list = [].concat(__app.schoolPictureList);
        _list.shift();
        return _list;

    }

    function _onCreateClick() {

        var _file;
        _fileInput = _fileInput || _getInitFileInput() ;
        _fileInput.click();

        function _getInitFileInput() {
            _fileInput = document.createElement("input");
            _fileInput.type = "file";
            _fileInput.onchange = _onChange;
            return _fileInput;
        }

        function _onChange(_event) {
            _file = _event.target.files[0];
            _previewImg(null, _file, _onPreview);
        }

        function _onPreview(_result) {
            __app.$popup.picture(_result, _onConfirm);
        }

        function _onConfirm() {
            __app.$popup.close("picture");
            __app.$popup.loading();
            __claim("post", "school/{id}/image", {
                    "{id}": __app.cmsSchoolID,
                    "file": _file,
                    "_method": "PUT"
                }, _onSuccess, null, _onFinish);
        }

        function _onSuccess(_response) {
            var _fileURLDictionary = _response.image_links;
            __app.schoolPictureList.push(_fileURLDictionary);
        }

        function _onFinish() {
            __app.$popup.close("loading");
        }

    }

    function _previewImg(_modelName, _file, _callback) {

        var _fileReader = new FileReader();
        _fileReader.onload = _onFileReadLoaded;
        _fileReader.readAsDataURL(_file);

        function _onFileReadLoaded() {
            var _result = _fileReader.result;
            if ( _modelName ) __app[_modelName] = _result;
            if ( _callback ) _callback(_result);
        }

    }


}(window.allot));