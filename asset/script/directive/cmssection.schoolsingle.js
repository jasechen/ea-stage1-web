(function (_allot) {

    "use strict";

    var __query = _allot("query");
    var __claim = _allot("claim");
    var __app = _allot("app");

    var _queries;
    var _tempFile;

    (function () {

        _queries = __query.all();
        _allot("cmsschoolsingle-directive", _componentDirective);
        _allot("cmsschoolsingle-directive-mixin", _definedData());

    }());

    function _componentDirective(_resolve) {

        __app = _allot("app");
        __claim("GET", "/template/cmssection.schoolsingle.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                inherit: true,
                    data: function () {
                            return __app.$data;
                        },
                    template: _response,
                    beforeCreate: _beforeCreate,
                    mounted: _mounted,
                    computed: {
                            filterCountryLocale: _filterLocaleGetter("filterCountryList", "filterCountry"),
                            filterLocationLocale: _filterLocaleGetter("filterLocationList", "filterLocation"),
                            filterSchooltypeLocale: _filterLocaleGetter("filterSchooltypeList", "filterSchooltype"),
                            filterProgramtypeLocale: _filterLocaleGetter("filterProgramtypeList", "filterProgramtypetype"),
                            filterProgramtaLocale: _filterLocaleGetter("filterProgramtaList", "filterProgramta")
                        },
                    methods: {
                            onModifyClick: _onModifyClick,
                            onModifyCancel: _onModifyCancel,
                            onCoverChange: _onCoverChange,
                            onUpdateClick: _onUpdateClick,
                            onDeleteClick: _onDeleteClick
                        },
                    watch: {
                            filterCountry: _onCountryChange,
                            filterProgramtypeValueArray: _onFilterProgramtypeChange
                        }
                });
        }

    }

    function _definedData() {

        return {
                data: _procedure
            };

        function _procedure() {
            return {
                    cmsSchoolID: "",
                    schoolformSlug: "",
                    schoolformName: "",
                    schoolformInner: "",
                    schoolformExcerpt: "",
                    schoolformFacility: "",
                    schoolformCover: "",
                    schoolformCoverURL: "",
                    schoolformCoverTitle: "",
                    schoolformCoverAlt: "",
                    schoolformNameMETA: "",
                    schoolformExcerptMETA: "",
                    schoolformNameOG: "",
                    schoolformExcerptOG: "",
                    cmsCourseList: [],
                    filterProgramValueDictionary: {},
                    filterProgramtypeValueArray: [],
                    filterProgramValueArray: [],
                    filterCountryList: [],
                    filterCountry: _queries.flt_country || "" ,
                    filterLocationList: [],
                    filterLocation: _queries.flt_location || "" ,
                    filterSchooltypeList: [],
                    filterSchooltype: _queries.flt_schooltype || "" ,
                    filterProgramtypeList: [],
                    filterProgramtaList: [],
                    filterProgramta: "",
                    schoolPictureList: [],
                    cmsDormList: []
                };
        }

    }

    function _beforeCreate() {

        _defaultValue();

        function _defaultValue() {
            __app.cmsSchoolID = __app.slug;
            __app.schoolformSlug = "";
            __app.schoolformName = "";
            __app.schoolformInner = "";
            __app.schoolformExcerpt = "";
            __app.schoolformFacility = "";
            __app.schoolformCover = "";
            __app.schoolformCoverURL = "";
            __app.schoolformCoverTitle = "";
            __app.schoolformCoverAlt = "";
            __app.schoolformNameMETA = "";
            __app.schoolformExcerptMETA = "";
            __app.schoolformNameOG = "";
            __app.schoolformExcerptOG = "";
            __app.cmsCourseList = [];
            __app.filterProgramtypeValueArray = [];
        }

    }

    function _mounted() {

        __app.$popup.loading();
        _readSingleDatum();
        _readCountryList();

        function _readCountryList() {
            __claim("GET", "filter/school/type/country", null, _onCountrySuccess);
        }

        function _readSchooltypeList() {
            __claim("GET", "filter/school/type/school-type", null, _onSchooltypeSuccess);
        }

        function _readProgramtypeList() {
            __claim("GET", "filter/school/type/program-type", null, _onProgramtypeSuccess);
        }

        function _readProgramtaList() {
            __claim("GET", "filter/school/type/program-ta", null, _onProgramtaSuccess);
        }

        function _onCountrySuccess(_response) {
            __app.filterCountryList = _response.filters;
            _readSchooltypeList();
        }

        function _onSchooltypeSuccess(_response) {
            __app.filterSchooltypeList = _response.filters;
            _readProgramtypeList();
        }

        function _onProgramtypeSuccess(_response) {
            __app.filterProgramtypeList = _response.filters;
            _readProgramtaList();
        }

        function _onProgramtaSuccess(_response) {
            __app.filterProgramtaList = _response.filters;
        }

    }

    function _readSingleDatum() {

        __claim("GET", "school/{id}", {
                "{id}": __app.cmsSchoolID
            }, _onSuccess, _onError);

        __claim("GET", "school/{id}/filter", {
                "{id}": __app.cmsSchoolID
            }, _onFilterSuccess);

        function _onSuccess(_response) {
            var _schoolDatum = _response.school;
            __app.schoolformSlug = _schoolDatum.slug;
            __app.schoolformName = _schoolDatum.name;
            __app.schoolformInner = _schoolDatum.description;
            __app.schoolformExcerpt = _schoolDatum.excerpt;
            __app.schoolformFacility = _schoolDatum.facility;
            __app.schoolformCover = _schoolDatum.cover;
            __app.schoolformCoverURL = _allot("path").API + ( _schoolDatum.cover_links.c || _schoolDatum.cover_links.o ) ;
            __app.schoolformCoverTitle = _schoolDatum.cover_title;
            __app.schoolformCoverAlt = _schoolDatum.cover_alt;
            __app.schoolformNameMETA = _schoolDatum.meta_title;
            __app.schoolformExcerptMETA = _schoolDatum.meta_description;
            __app.schoolformNameOG = _schoolDatum.og_title;
            __app.schoolformExcerptOG = _schoolDatum.og_description;
            __app.schoolformExcerptOG = _schoolDatum.og_description;
            __app.schoolPictureList = _schoolDatum.image_links;
            _readCourseList();
        }

        function _onFilterSuccess(_response) {
            var _list = _response.filders;
            var _item;
            var _tempArray;
            while ( _list.length ) {
                _item = _list.shift();
                if ( _item.parent_type === "country" ) __app.filterCountry = _item.id;
                if ( _item.parent_type === "location" ) __app.filterLocation = _item.id;
                if ( _item.parent_type === "school-type" ) __app.filterSchooltype = _item.id;
                if ( _item.parent_type === "program-type" ) __app.filterProgramtypeValueArray.push(_item.id);
                if ( _item.parent_type === "program" ) __app.filterProgramValueArray.push(_item.id);
                if ( _item.parent_type === "program-ta" ) __app.filterProgramta = _item.id;
            }
        }

        function _onError() {
            __app.$popup.close("loading");
        }

    }

    function _readCourseList() {

        __claim("get", "school/{id}/course", {
                "{id}": __app.cmsSchoolID
            }, _onSuccess, null, _onFinish);

        function _onSuccess(_response) {
            __app.cmsCourseList = _response.courses;
        }

        function _onFinish() {
            _readDormList();
        }

    }

    function _readDormList() {

        __claim("get", "school/{id}/dorm", {
                "{id}": __app.cmsSchoolID
            }, _onSuccess, null, _onFinish);

        function _onSuccess(_response) {
            __app.cmsDormList = _response.school_dorms;
        }

        function _onFinish() {
            __app.$popup.close("loading");
        }

    }

    function _filterLocaleGetter(_listName, _valueName) {

        var _index;
        var _item;
        return _procedure;

        function _procedure() {
            _index = ( __app[_listName] || [] ).length;
            while ( _index-- ) {
                _item = ( __app[_listName] || [] )[_index];
                if ( _item.id === __app[_valueName] ) return ( _item.code_langs || {} ).tw || _item.code ;
            }
            return "";
        }

    }

    function _onModifyClick() {

        _changeModifyStat();

    }

    function _onModifyCancel() {

        window.location.reload();

    }

    function _onCoverChange(_event) {

        __app.$popup.loading();
        _tempFile = _event.target.files[0];
        __claim("post", "school/cover", {
                "file": _tempFile
            }, _onSuccess, null, _onFinish);

        function _onSuccess(_response) {
            __app.schoolformCover = _response.filename;
            _previewImg("schoolformCoverURL", _tempFile);
        }

        function _onFinish() {
            __app.$popup.close("loading");
        }

    }

    function _onUpdateClick() {

        __app.$popup.loading();
        _updateSingle();

    }

    function _onDeleteClick() {

        __app.$popup.confirm("確認刪除？", _onConfirmDelete);

        function _onConfirmDelete() {
            __app.$popup.close("confirm");
            __app.$popup.loading();
            __claim("DELETE", "school/{id}", {
                    "{id}": __app.cmsSchoolID
                }, _onSuccess, _onError, _onFinish);
        }

        function _onSuccess() {
            __app.$popup.alert("刪除成功", _onConfirmSuccess);
        }

        function _onError() {
            __app.$popup.alert("刪除失敗");
        }

        function _onFinish() {
            __app.$popup.close("loading");
        }

        function _onConfirmSuccess() {
            __app.$popup.close("alert");
            window.location.hash = "#/schoollist";
        }

    }

    function _onCountryChange() {

        __claim("GET", "filter/school/type/location/id/{parent_id}", {
                "{parent_id}": __app.filterCountry
            }, _onSuccess);

        function _onSuccess(_response) {
            __app.filterLocationList = _response.filters;
        }

    }

    function _onFilterProgramtypeChange(_list, _originList) {

        var _requiredID;
        var _requiredArray = [];
        var _index;
        var _item;
        if ( _list === _originList ) _originList = [];
        _inspectRemoveValue();
        _inspectRquiredID();
        if ( _requiredID ) _readProgram(_requiredID);

        function _inspectRemoveValue() {
            _index = _originList.length;
            while ( _index-- ) {
                _item = _originList[_index];
                if ( _list.indexOf(_item) < 0 ) delete __app.filterProgramValueDictionary[_item];
            }
        }

        function _inspectRquiredID() {
            var _id;
            _index = _list.length;
            while ( _index-- ) {
                _id = _list[_index];
                if ( _originList.indexOf(_id) >= 0 ) continue;
                _requiredArray.push(_id);
                _requiredID = _id;
            }
        }

        function _readProgram() {
            _requiredID = _requiredArray.shift();
            __claim("GET", "filter/school/type/program/id/{parent_id}", {
                    "{parent_id}": _requiredID
                }, _onSuccess);
        }

        function _onSuccess(_response) {
            __app.filterProgramValueDictionary[_requiredID] = ( _response || {} ).filters || [] ;
            __app.filterProgramValueDictionary = JSON.parse(JSON.stringify(__app.filterProgramValueDictionary));
            if ( _requiredArray.length ) _readProgram();
        }

    }

    function _updateSingle() {

        __claim("put", "school/{id}", {
                "{id}": __app.cmsSchoolID,
                "slug": __app.schoolformSlug,
                "name": __app.schoolformName,
                "description": __app.schoolformInner,
                "facility": __app.schoolformFacility,
                "cover": __app.schoolformCover
            }, null, null, _onFinish);

        function _onFinish() {
            _updateSEO();
        }

    }

    function _updateSEO() {

        __claim("put", "school/{id}/seo", {
                "{id}": __app.cmsSchoolID,
                "slug": __app.schoolformSlug,
                "excerpt": __app.schoolformExcerpt,
                "cover_title": __app.schoolformCoverTitle,
                "cover_alt": __app.schoolformCoverAlt,
                "meta_title": __app.schoolformNameMETA,
                "meta_description": __app.schoolformExcerptMETA,
                "og_title": __app.schoolformNameOG,
                "og_description": __app.schoolformExcerptOG
            }, null, null, _onFinish);

        function _onFinish() {
            _updateFilter();
        }

    }

    function _updateFilter() {

        __claim("PUT", "school/{id}/filter", {
                "{id}": __app.cmsSchoolID,
                "filter_ids": _getFilterIDArray()
            }, null, null, _onFinish);

        function _onFinish() {
            __app.$popup.close("loading");
            __app.$popup.alert("更新成功", _onConfirmFinish);
        }

        function _onConfirmFinish() {
            window.location.reload();
        }

        function _getFilterIDArray() {
            var _array = [];
            if ( __app.filterCountry ) _array.push(__app.filterCountry);
            if ( __app.filterLocation ) _array.push(__app.filterLocation);
            if ( __app.filterSchooltype ) _array.push(__app.filterSchooltype);
            if ( __app.filterProgramta ) _array.push(__app.filterProgramta);
            _array = _array.concat(__app.filterProgramtypeValueArray);
            _array = _array.concat(__app.filterProgramValueArray);
            return _array;
        }

    }

    function _changeModifyStat() {

        __app.cmsmodify = !(__app.cmsmodify);

    }

    function _previewImg(_modelName, _file) {

        var _fileReader = new FileReader();
        _fileReader.onload = _onFileReadLoaded;
        _fileReader.readAsDataURL(_file);

        function _onFileReadLoaded() {
            __app[_modelName] = _fileReader.result;
        }

    }

}(window.allot));