(function (_allot) {

    "use strict";

    var __claim = _allot("claim");
    var __app;

    (function () {

        _allot("cmsrecommendsingle-directive", _componentDirective);

    }());

    function _componentDirective(_resolve) {

        __app = __app || _allot("app") ;
        __claim("GET", "/template/cmssection.recommendsingle.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                    data: function () {
                            return _allot("app").$data;
                        },
                    template: _response,
                    beforeCreate: _beforeCreate,
                    mounted: _mounted,
                    methods: {
                            onModifyClick: __app.onModifyClick,
                            onModifyCancel: __app.onModifyCancel,
                            onUpdateClick: _onUpdateClick,
                            onDeleteClick: _onDeleteClick,
                            onImageChange: _onImageChange
                        }
                });
        }

    }

    function _beforeCreate() {

        _allot("cmsrecommend-controller").initID();

    }

    function _mounted() {

        __app.$popup.loading();
        __app.agentID = _allot("query").getItem("agent");
        _allot("cmsrecommend-controller").initDatum();
        _allot("cmsrecommend-controller").fetchSingle(null, null, _onFinish);

        function _onFinish() {
            __app.$popup.close("loading");
        }

    }

    function _onUpdateClick() {

        __app.$popup.loading();
        _allot("cmsrecommend-controller").createImage(null, null, _onImageFinish);

        function _onImageFinish() {
            _allot("cmsrecommend-controller").updateSingle(null, null, _onSingleFinish);
        }

        function _onSingleFinish() {
            __app.$popup.close("loading");
            __app.$popup.alert("更新成功", _onConfirmFinish);
        }

        function _onConfirmFinish() {
            window.location.reload();
        }

    }

    function _onDeleteClick() {

        __app.$popup.confirm("確認刪除？", _onConfirmDelete);

        function _onConfirmDelete() {
            __app.$popup.close("confirm");
            __app.$popup.loading();
            _allot("cmsrecommend-controller").deleteSingle(_onSuccess, _onError, _onFinish);
        }

        function _onSuccess() {
            __app.$popup.alert("刪除成功", _onConfirmSuccess);
        }

        function _onError() {
            __app.$popup.alert("刪除失敗");
        }

        function _onFinish() {
            __app.$popup.close("loading");
        }

        function _onConfirmSuccess() {
            __app.$popup.close("alert");
            window.location.hash = "#/agent/" + __app.agentID ;
        }

    }

    function _onImageChange(_event) {

        var _file = _event.target.files[0];
        var _fileReader = new FileReader();
        _fileReader.onload = _onFileReadLoaded;
        _fileReader.readAsDataURL(_file);

        function _onFileReadLoaded() {
            _allot("cmsrecommend-controller").updateImageURL(_fileReader.result);
        }

    }

}(window.allot));