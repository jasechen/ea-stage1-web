(function (_allot) {

    "use strict";

    var __claim = _allot("claim");
    var __app = _allot("app");

    (function () {

        _allot("cmscourselist-directive", _componentDirective);
        _allot("cmscourselist-directive-mixin", _definedData());

    }());

    function _componentDirective(_resolve) {

        __app = _allot("app");
        __claim("GET", "/template/cmssection.courselist.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                    data: function () {
                            return __app.$data;
                        },
                    template: _response,
                    mounted: _mounted,
                    methods: {
                        }
                });
        }

    }

    function _definedData() {

        return {
                data: _procedure
            };

        function _procedure() {
            return {
                };
        }

    }

    function _mounted() {



    }

}(window.allot));