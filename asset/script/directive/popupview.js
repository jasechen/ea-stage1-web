(function (_allot) {

    "use strict";

    var __cookie = _allot("cookie");
    var __claim = _allot("claim");
    var __app = _allot("app");

    (function () {

        _allot("popup-directive", _componentPopupView);
        _allot("popup-directive-mixin", _definedData());

    }());

    function _componentPopupView(_resolve) {

        __app = _allot("app");
        __claim("GET", "/template/popup.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                    data: function () {
                            return __app.$data;
                        },
                    template: _response,
                    methods: {
                            onPopupClick: _onPopupClick,
                            onPopupLoginSubmit: _onPopupLoginSubmit,
                            onPopupLoginConfirm: _onPopupLoginConfirm,
                            onPopupRegisterConfirm: _onPopupRegisterConfirm,
                            onPopupPromptConfirm: _onPopupPromptConfirm,
                            onPopupPictureConfirm: _onPopupPictureConfirm,
                            onPopupConfirmConfirm: _onPopupConfirmConfirm,
                            onPopupAlertConfirm: _onPopupAlertConfirm,
                            onGotoRegister: _onGotoRegister
                        }
                });
        }

    }

    function _definedData() {

        return {
                data: _procedure
            };

        function _procedure() {
            return {
                    popupstats: [],
                    popupPromptMessage: "",
                    popupPromptValue: "",
                    popupConfirmMessage: "",
                    popupPictureURL: "",
                    popupAlertMessage: "",
                    loginAccount: "",
                    loginPassword: "",
                    registerAccount: "",
                    registerPassword: "",
                    registerRepassword: "",
                    registerFirstname: "",
                    registerLastname: "",
                    registerPhone: ""
                };
        }

    }

    function _onPopupClick(_event) {

        switch ( true ) {
            case /(^|\s)cancel(\s|$)/.test(_event.target.className):
            __app.$popup.close();
        }

    }

    function _onPopupLoginSubmit(_event) {

        _event.preventDefault();
        _onPopupLoginConfirm();

    }

    function _onPopupLoginConfirm() {

        __app.$popup.loading();
        __claim("POST", "login", {
                account: __app.loginAccount,
                password: __app.loginPassword
            }, _onSuccess, _onError, _onFinish);

        function _onSuccess(_response) {
            __app.lifetoken = _response.token;
            __cookie.addItem("LIFETOKEN", __app.lifetoken);
            __app.$popup.methods.popupLoginConfirm();
            __app.$popup.close("login");
            window.location.reload();
        }

        function _onError(_response) {
            if ( _response.comment === "user status error" ) {
                __app.lifetoken = ( _response.data || {} ).token;
                __cookie.addItem("LIFETOKEN", __app.lifetoken);
                __app.$popup.alert("帳號尚未驗證完成", _gotoVerify);
            } else {
                __app.loginPassword = "";
                __app.$popup.alert("帳號與密碼有誤");
            }
        }

        function _onFinish() {
            __app.$popup.close("loading");
        }

        function _gotoVerify() {
            window.location.href = "/cms/";
        }

    }

    function _onPopupRegisterConfirm() {

        __app.$popup.loading();
        __claim("POST", "register", {
                account: __app.registerAccount,
                password: __app.registerPassword,
                password_repeat: __app.registerRepassword,
                first_name: __app.registerFirstname,
                last_name: __app.registerLastname,
                mobile_country_code: "886",
                mobile_phone: _getInspectedPhone(__app.registerPhone)
            }, _onSuccess, _onError, _onFinish);

        function _onSuccess(_response) {
            __app.lifetoken = _response.token;
            __cookie.addItem("LIFETOKEN", __app.lifetoken);
            __app.$popup.methods.popupRegisterConfirm();
            __app.$popup.alert("帳號註冊成功請進行驗證", _gotoVerify);
        }

        function _onError() {
            __app.$popup.alert("資料有誤");
        }

        function _onFinish() {
            __app.$popup.close("loading");
        }

        function _getInspectedPhone(_phone) {
            return _phone.replace(/^0/, "");
        }

        function _gotoVerify() {
            window.location.href = "/cms/";
        }

    }

    function _onPopupPromptConfirm() {

        __app.$popup.methods.popupPromptConfirm();

    }

    function _onPopupPictureConfirm() {

        __app.$popup.methods.popupPictureConfirm();

    }

    function _onPopupConfirmConfirm() {

        __app.$popup.methods.popupConfirmConfirm();

    }

    function _onPopupAlertConfirm() {

        __app.$popup.methods.popupAlertConfirm();

    }

    function _onGotoRegister() {

        __app.$popup.register();

    }

}(window.allot));