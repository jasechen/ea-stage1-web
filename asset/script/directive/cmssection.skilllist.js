(function (_allot) {

    "use strict";

    var __claim = _allot("claim");

    (function () {

        _allot("cmsskilllist-directive", _componentDirective);

    }());

    function _componentDirective(_resolve) {

        __claim("GET", "/template/cmssection.skilllist.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                    data: function () {
                            return _allot("app").$data;
                        },
                    template: _response,
                    mounted: _mounted,
                    methods: {
                        }
                });
        }

    }

    function _mounted() {

        _allot("cmsskill-controller").fetchList();

    }

}(window.allot));