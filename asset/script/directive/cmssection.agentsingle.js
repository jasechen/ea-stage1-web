(function (_allot) {

    "use strict";

    var __claim = _allot("claim");
    var __app = _allot("app");

    (function () {

        _allot("cmsagentsingle-directive", _componentDirective);

    }());

    function _componentDirective(_resolve) {

        __app = _allot("app");
        __claim("GET", "/template/cmssection.agentsingle.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                    inherit: true,
                    data: function () {
                            return __app.$data;
                        },
                    template: _response,
                    beforeCreate: _beforeCreate,
                    mounted: _mounted,
                    methods: {
                            onModifyClick: __app.onModifyClick,
                            onModifyCancel: __app.onModifyCancel,
                            onUpdateClick: _onUpdateClick,
                            onDeleteClick: _onDeleteClick,
                            onAvatarChange: _onAvatarChange
                        }
                });
        }

    }

    function _beforeCreate() {

        _allot("cmsagent-controller").initID();

    }

    function _mounted() {

        _readAgent();
        _readFilterCountries();

    }

    function _readAgent() {

        __app.$popup.loading();
        _allot("cmsagent-controller").initDatum();
        _allot("cmsagent-controller").fetchSingle(null, null, _onFinish);

        function _onFinish() {
            __app.$popup.close("loading");
        }

    }

    function _readFilterCountries() {

        __claim("get", "filter/school/type/country", {
            }, _onSuccess, null, _onFinish);

        function _onSuccess(_response) {
            __app.targetCountryList = _response.filters || [] ;
        }

        function _onFinish() {
            _readFilterLocations();
        }

    }

    function _readFilterLocations() {

        __claim("get", "user/agent/profile/locations", {
            }, _onSuccess);

        function _onSuccess(_response) {
            __app.locationDictionary = _response.locations || {} ;
        }

    }

    function _onUpdateClick() {

        __app.$popup.loading();
        _allot("cmsagent-controller").updateSingle(_onSuccess, _onError, _onFinish);

        function _onSuccess() {
            __app.$popup.alert("更新成功", _onConfirmSuccess);
        }

        function _onError() {
            __app.$popup.alert("資料有誤");
        }

        function _onFinish() {
            __app.$popup.close("loading");
        }

        function _onConfirmSuccess() {
            window.location.reload();
        }

    }

    function _onDeleteClick() {

        __app.$popup.confirm("確認刪除？", _onConfirmDelete);

        function _onConfirmDelete() {
            __app.$popup.close("confirm");
            __app.$popup.loading();
            _allot("cmsagent-controller").deleteSingle(_onSuccess, _onError, _onFinish);
        }

        function _onSuccess() {
            __app.$popup.alert("刪除成功", _onConfirmSuccess);
        }

        function _onError() {
            __app.$popup.alert("刪除失敗");
        }

        function _onFinish() {
            __app.$popup.close("loading");
        }

        function _onConfirmSuccess() {
            __app.$popup.close("alert");
            window.location.hash = "#/agentlist";
        }

    }

    function _onAvatarChange(_event) {

        var _file = _event.target.files[0];
        var _fileReader = new FileReader();
        _fileReader.onload = _onFileReadLoaded;
        _fileReader.readAsDataURL(_file);

        function _onFileReadLoaded() {
            _allot("cmsagent-controller").updateAvatarURL(_fileReader.result);
        }

    }

}(window.allot));