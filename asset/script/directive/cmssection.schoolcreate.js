(function (_allot) {

    "use strict";

    var __query = _allot("query");
    var __claim = _allot("claim");
    var __app = _allot("app");

    var _queries;
    var _tempFile;

    (function () {

        _queries = __query.all();
        _allot("cmsschoolcreate-directive", _componentDirective);
        _allot("cmsschoolcreate-directive-mixin", _definedData());

    }());

    function _componentDirective(_resolve) {

        __app = _allot("app");
        __claim("GET", "/template/cmssection.schoolcreate.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                    inherit: true,
                    data: function () {
                            return __app.$data;
                        },
                    template: _response,
                    beforeCreate: _beforeCreate,
                    mounted: _mounted,
                    methods: {
                            onCreateClick: _onCreateClick,
                            onCoverChange: _onCoverChange
                        },
                    watch: {
                            filterCountry: _onCountryChange,
                            filterProgramtypeValueArray: _onFilterProgramtypeChange
                        }
                });
        }

    }

    function _definedData() {

        return {
                data: _procedure
            };

        function _procedure() {
            return {
                    cmsSchoolID: "",
                    schoolformSlug: "",
                    schoolformName: "",
                    schoolformInner: "",
                    schoolformExcerpt: "",
                    schoolformFacility: "",
                    schoolformCover: "",
                    schoolformCoverURL: "",
                    schoolformCoverTitle: "",
                    schoolformCoverAlt: "",
                    schoolformNameMETA: "",
                    schoolformExcerptMETA: "",
                    schoolformNameOG: "",
                    schoolformExcerptOG: "",
                    filterProgramValueDictionary: {},
                    filterProgramtypeValueArray: [],
                    filterProgramValueArray: [],
                    filterCountryList: [],
                    filterCountry: _queries.flt_country || "",
                    filterLocationList: [],
                    filterLocation: _queries.flt_location || "" ,
                    filterSchooltypeList: [],
                    filterSchooltype: _queries.flt_schooltype || "",
                    filterProgramtypeList: [],
                    filterProgramtaList: [],
                    filterProgramta: ""
                };
        }

    }

    function _beforeCreate() {

        _defaultValue();

        function _defaultValue() {
            __app.schoolformSlug = "";
            __app.schoolformName = "";
            __app.schoolformInner = "";
            __app.schoolformExcerpt = "";
            __app.schoolformFacility = "";
            __app.schoolformCover = "";
            __app.schoolformCoverURL = "";
            __app.schoolformCoverTitle = "";
            __app.schoolformCoverAlt = "";
            __app.schoolformNameMETA = "";
            __app.schoolformExcerptMETA = "";
            __app.schoolformNameOG = "";
            __app.schoolformExcerptOG = "";
            __app.filterProgramValueDictionary = {};
            __app.filterProgramtypeValueArray = [];
            __app.filterProgramValueArray = [];
        }

    }

    function _mounted() {

        _readCountryList();

        function _readCountryList() {
            __claim("GET", "filter/school/type/country", null, _onCountrySuccess);
        }

        function _readSchooltypeList() {
            __claim("GET", "filter/school/type/school-type", null, _onSchooltypeSuccess);
        }

        function _readProgramtypeList() {
            __claim("GET", "filter/school/type/program-type", null, _onProgramtypeSuccess);
        }

        function _readProgramtaList() {
            __claim("GET", "filter/school/type/program-ta", null, _onProgramtaSuccess);
        }

        function _onCountrySuccess(_response) {
            __app.filterCountryList = _response.filters;
            _readSchooltypeList();
        }

        function _onSchooltypeSuccess(_response) {
            __app.filterSchooltypeList = _response.filters;
            _readProgramtypeList();
        }

        function _onProgramtypeSuccess(_response) {
            __app.filterProgramtypeList = _response.filters;
            _readProgramtaList();
        }

        function _onProgramtaSuccess(_response) {
            __app.filterProgramtaList = _response.filters;
        }

    }

    function _onCreateClick() {

        _createSchool();

    }

    function _createSchool() {

        __app.$popup.loading();
        _procedure();

        function _procedure() {
            if ( _inspectSchoolFormEmpty() ) _onError();
            else __claim("post", "school", {
                    "slug": __app.schoolformSlug,
                    "name": __app.schoolformName,
                    "description": __app.schoolformInner,
                    "excerpt": __app.schoolformExcerpt,
                    "facility": __app.schoolformFacility,
                    "cover": __app.schoolformCover,
                    "cover_title": __app.schoolformCoverTitle,
                    "cover_alt": __app.schoolformCoverAlt,
                    "meta_title": __app.schoolformNameMETA,
                    "meta_description": __app.schoolformExcerptMETA,
                    "og_title": __app.schoolformNameOG,
                    "og_description": __app.schoolformExcerptOG
                }, _onSchoolSuccess, _onError);
        }

        function _updateFilter() {
            __claim("PUT", "school/{id}/filter", {
                    "{id}": __app.cmsSchoolID,
                    "filter_ids": _getFilterIDArray()
                }, null, null, _onFilterFinish);
        }

        function _onSchoolSuccess(_response) {
            __app.cmsSchoolID = _response.school_id;
            _updateFilter();
        }

        function _onError() {
            __app.$popup.close("loading");
            __app.$popup.alert("資料有誤");
        }

        function _onFilterFinish() {
            __app.$popup.close("loading");
            __app.$popup.alert("新增成功", _confirmFinish);
        }

        function _confirmFinish() {
            __app.$popup.close("alert");
            window.location.hash = "#/school/" + __app.cmsSchoolID ;
        }

        function _inspectSchoolFormEmpty() {
            if ( !(__app.schoolformSlug) ) return true;
            if ( !(__app.schoolformName) ) return true;
            if ( !(__app.schoolformInner) ) return true;
            if ( !(__app.schoolformExcerpt) ) return true;
            if ( !(__app.schoolformFacility) ) return true;
            if ( !(__app.schoolformCover) ) return true;
            if ( !(__app.schoolformCoverTitle) ) return true;
            if ( !(__app.schoolformCoverAlt) ) return true;
            if ( !(__app.schoolformNameMETA) ) return true;
            if ( !(__app.schoolformExcerptMETA) ) return true;
            if ( !(__app.schoolformNameOG) ) return true;
            if ( !(__app.schoolformExcerptOG) ) return true;
        }

        function _getFilterIDArray() {
            var _array = [];
            if ( __app.filterCountry ) _array.push(__app.filterCountry);
            if ( __app.filterLocation ) _array.push(__app.filterLocation);
            if ( __app.filterSchooltype ) _array.push(__app.filterSchooltype);
            if ( __app.filterProgramta ) _array.push(__app.filterProgramta);
            _array = _array.concat(__app.filterProgramtypeValueArray);
            _array = _array.concat(__app.filterProgramValueArray);
            return _array;
        }

    }

    function _onCoverChange(_event) {

        __app.$popup.loading();
        _tempFile = _event.target.files[0];
        __claim("post", "school/cover", {
                "file": _tempFile
            }, _onSuccess, null, _onFinish);

        function _onSuccess(_response) {
            __app.schoolformCover = _response.filename;
            _previewImg("schoolformCoverURL", _tempFile);
        }

        function _onFinish() {
            __app.$popup.close("loading");
        }


    }

    function _onCountryChange() {

        __claim("GET", "filter/school/type/location/id/{parent_id}", {
                "{parent_id}": __app.filterCountry
            }, _onSuccess);

        function _onSuccess(_response) {
            __app.filterLocationList = _response.filters;
        }

    }

    function _onFilterProgramtypeChange(_list, _originList) {

        var _requiredID;
        var _index;
        var _item;
        _inspectRemoveValue();
        _inspectRquiredID();
        if ( _requiredID ) _readProgram(_requiredID);

        function _inspectRemoveValue() {
            _index = _originList.length;
            while ( _index-- ) {
                _item = _originList[_index];
                if ( _list.indexOf(_item) < 0 ) delete __app.filterProgramValueDictionary[_item];
            }
        }

        function _inspectRquiredID() {
            var _id;
            _index = _list.length;
            while ( _index-- ) {
                _id = _list[_index];
                if ( _originList.indexOf(_id) >= 0 ) continue;
                _requiredID = _id;
                break;
            }
        }

        function _readProgram() {
            __claim("GET", "filter/school/type/program/id/{parent_id}", {
                    "{parent_id}": _requiredID
                }, _onSuccess);
        }

        function _onSuccess(_response) {
            __app.filterProgramValueDictionary[_requiredID] = ( _response || {} ).filters || [] ;
            __app.filterProgramValueDictionary = JSON.parse(JSON.stringify(__app.filterProgramValueDictionary));
        }

    }

    function _previewImg(_modelName, _file) {

        var _fileReader = new FileReader();
        _fileReader.onload = _onFileReadLoaded;
        _fileReader.readAsDataURL(_file);

        function _onFileReadLoaded() {
            __app[_modelName] = _fileReader.result;
        }

    }

}(window.allot));