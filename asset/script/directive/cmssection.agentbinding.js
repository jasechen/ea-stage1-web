(function (_allot) {

    "use strict";

    var __claim = _allot("claim");
    var __app = _allot("app");

    (function () {

        _allot("cmsagentbinding-directive", _componentDirective);

    }());

    function _componentDirective(_resolve) {

        __app = _allot("app");
        __claim("GET", "/template/cmssection.agentbinding.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                inherit: true,
                    data: function () {
                            return __app.$data;
                        },
                    template: _response,
                    mounted: _mounted,
                    methods: {
                            onBindingClick: _onBindingClick
                        }
                });
        }

    }

    function _mounted() {

        if ( !(__app.cmsSchoolID) ) __app.cmsSchoolID = __app.slug;
        _allot("cmsagent-controller").fetchList();
        _readAgentRole();
        __claim("GET", "school/{id}/user", {
                "{id}": __app.cmsSchoolID
            }, _onSuccess);

        function _onSuccess(_response) {
            var _responseUsers = _response.school_users || [] ;
            var _item;
            __app.agentBindingArray = JSON.parse(JSON.stringify(_responseUsers));
            __app.agentBindingIDArray = [];
            while ( _responseUsers.length ) {
                _item = _responseUsers.shift();
                __app.agentBindingIDArray.push(_item.user_id);
            }
        }

    }

    function _readAgentRole() {

        _allot("role-controller").fetchSingle("company", "agent", _onSuccess);

        function _onSuccess(_response) {
            __app.agentDefaultRoleID = _response.id;
        }

    }

    function _onBindingClick(_event, _item) {

        _event.preventDefault();
        if ( __app.agentBindingIDArray.indexOf(_item.id) < 0 ) __app.$popup.confirm("綁定？", _onCreateConfirm);
        else __app.$popup.confirm("取消綁定？", _onDeleteConfirm);

        function _onCreateConfirm() {
            __app.$popup.close("confirm");
            __app.$popup.loading();
            __claim("POST", "school/user", {
                "school_id": __app.cmsSchoolID,
                "user_id": _item.id,
                "role_id": __app.agentDefaultRoleID
            }, _onCreateSuccess, _onError, _onFinish);
        }

        function _onDeleteConfirm() {
            var _index = __app.agentBindingArray.length;
            var _tmpItem;
            __app.$popup.close("confirm");
            __app.$popup.loading();
            while ( _index-- ) {
                _tmpItem = __app.agentBindingArray[_index];
                if ( _tmpItem.user_id !== _item.id ) continue;
                return __claim("DELETE", "school/user/{id}", {
                        "{id}": _tmpItem.id
                    }, _onDeleteSuccess, _onError, _onFinish);
            }
        }

        function _onCreateSuccess(_response) {
            __app.agentBindingArray.push({
                    id: _response.school_user_id,
                    user_id: _item.id
                });
            __app.agentBindingIDArray.push(_item.id);
            __app.$popup.alert("綁定成功");
        }

        function _onDeleteSuccess() {
            __app.agentBindingIDArray.splice(__app.agentBindingIDArray.indexOf(_item.id), 1);
            __app.$popup.alert("取消綁定成功");
        }

        function _onError() {
            __app.$popup.alert("錯誤");
        }

        function _onFinish() {
            __app.$popup.close("loading");
        }

    }

}(window.allot));