(function (_allot) {

    "use strict";

    var __query = _allot("query");
    var __claim = _allot("claim");
    var __app = _allot("app");

    (function () {

        _allot("cmscoursecreate-directive", _componentDirective);
        _allot("cmscoursecreate-directive-mixin", _definedData());

    }());

    function _componentDirective(_resolve) {

        __app = _allot("app");
        __claim("GET", "/template/cmssection.coursecreate.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                    data: function () {
                            return __app.$data;
                        },
                    template: _response,
                    beforeCreate: _beforeCreate,
                    mounted: _mounted,
                    methods: {
                            onCourseCreateClick: _onCourseCreateClick
                        }
                });
        }

    }

    function _definedData() {

        return {
                data: _procedure
            };

        function _procedure() {
            return {
                    cmsSchoolID: "",
                    courseName: "",
                    courseExcerpt: "",
                    courseTA: ""
                };
        }

    }

    function _beforeCreate() {

        __app.cmsSchoolID = __app.cmsSchoolID || __query.all().school ;
        __app.courseName = "";
        __app.courseExcerpt = "";
        __app.courseTA = "adult";

    }

    function _mounted() {



    }

    function _onCourseCreateClick() {

        __claim("post", "school/course", {
                "school_id": __app.cmsSchoolID,
                "name": __app.courseName,
                "description": __app.courseExcerpt,
                "ta": __app.courseTA
            }, _onSuccess, _onError);

        function _onSuccess() {
            __app.$popup.alert("新增成功", _confirmSuccess);
        }

        function _confirmSuccess() {
            __app.$popup.close("alert");
            window.location.hash = "#/school/" + __app.cmsSchoolID ;
        }

        function _onError() {
            __app.$popup.alert("資料有誤");
        }

    }

}(window.allot));