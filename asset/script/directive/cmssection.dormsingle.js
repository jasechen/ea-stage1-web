(function (_allot) {

    "use strict";

    var __query = _allot("query");
    var __claim = _allot("claim");
    var __app = _allot("app");

    var _dormID;

    (function () {

        _allot("cmsdormsingle-directive", _componentDirective);
        _allot("cmsdormsingle-directive-mixin", _definedData());

    }());

    function _componentDirective(_resolve) {

        __app = _allot("app");
        __claim("GET", "/template/cmssection.dormsingle.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                inherit: true,
                    data: function () {
                            __app.$data.ROOM_TYPE_DICTIONARY = [
                                    "single",
                                    "double",
                                    "triple",
                                    "quadruple",
                                    "five-people",
                                    "six-people",
                                    "eight-people",
                                    "semi-double",
                                    "twin",
                                    "suit-2",
                                    "suit-3",
                                    "suit-4"
                                ];
                            return __app.$data;
                        },
                    template: _response,
                    beforeCreate: _beforeCreate,
                    mounted: _mounted,
                    methods: {
                            onModifyClick: _onModifyClick,
                            onModifyCancel: _onModifyCancel,
                            onUpdateClick: _onUpdateClick,
                            onRoomDeleteClick: _onRoomDeleteClick
                        },
                    watch: {
                            dormDefaultRoomType: _onDefaultRoomTypeChange,
                        }
                });
        }

    }

    function _definedData() {

        return {
                data: _procedure
            };

        function _procedure() {
            return {
                    cmsSchoolID: "",
                    dormType: "",
                    dormTypeLocale: "",
                    dormService: "",
                    dormFacility: "",
                    dormRoomList: [],
                    dormDefaultRoomType: ""
                };
        }

    }

    function _beforeCreate() {

        __app.cmsSchoolID = __app.cmsSchoolID || __query.all().school ;
        __app.dormType = "";
        __app.dormTypeLocale = "";
        __app.dormService = "";
        __app.dormFacility = "";
        __app.dormRoomList = [];
        __app.dormDefaultRoomType = "";
        _dormID = __app.slug;

    }

    function _mounted() {

        __app.$popup.loading();
        __claim("GET", "school/dorm/{school_dorm_id}", {
                "{school_dorm_id}": _dormID
            }, _onSuccess, null, _onFinish);

        function _onSuccess(_response) {
            var _dormDatum = _response.school_dorm;
            __app.dormType = _dormDatum.type;
            __app.dormTypeLocale = ( _dormDatum.type_langs || {} ).tw;
            __app.dormService = _dormDatum.service;
            __app.dormFacility = _dormDatum.facility;
            __app.dormRoomList = _getInspectedRoomList(_dormDatum.rooms);
        }

        function _onFinish() {
            __app.$popup.close("loading");
        }

        function _getInspectedRoomList(_list) {
            var _index = _list.length;
            var _item;
            while ( _index-- ) {
                _item = _list[_index];
                _item.accessible = !!JSON.parse( _item.accessible || false );
                _item.smoking = !!JSON.parse( _item.smoking || false );
            }
            return _list;
        }

    }

    function _onModifyClick() {

        _changeModifyStat();

    }

    function _onModifyCancel() {

        window.location.reload();

    }

    function _onUpdateClick() {

        __app.$popup.loading();
        __claim("PUT", "school/dorm/{school_dorm_id}", {
                "{school_dorm_id}": _dormID,
                type: __app.dormType,
                service: __app.dormService,
                facility: __app.dormFacility,
                rooms: _getInspectRoomList(__app.dormRoomList)
            }, _onSuccess, _onError, _onFinish);

        function _onSuccess() {
            __app.$popup.alert("更新成功", _confirmSuccess);
        }

        function _confirmSuccess() {
            __app.$popup.close("alert");
            window.location.reload();
        }

        function _onError(_response) {
            _response = _response || {} ;
            if ( _response.comment === "updateData empty" ) _onSuccess();
            else __app.$popup.alert("資料有誤");
        }

        function _onFinish() {
            __app.$popup.close("loading");
        }

        function _getInspectRoomList(_list) {
            var _index = _list.length;
            var _item;
            while ( _index-- ) {
                _item = _list[_index];
                _item.accessible = String( _item.accessible || false );
                _item.smoking = String( _item.smoking || false );
            }
            return _list;
        }

    }

    function _onRoomDeleteClick(_index) {

        __app.dormRoomList.splice(_index, 1);

    }

    function _onDefaultRoomTypeChange() {

        if ( !(__app.dormDefaultRoomType) ) return;
        __app.dormRoomList.push({
                type: __app.dormDefaultRoomType
            });
        __app.dormDefaultRoomType = "";

    }

    function _changeModifyStat() {

        __app.cmsmodify = !(__app.cmsmodify);

    }

}(window.allot));