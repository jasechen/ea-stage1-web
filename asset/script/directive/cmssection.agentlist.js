(function (_allot) {

    "use strict";

    var __claim = _allot("claim");
    var __app = _allot("app");

    (function () {

        _allot("cmsagentlist-directive", _componentDirective);

    }());

    function _componentDirective(_resolve) {

        __app = _allot("app");
        __claim("GET", "/template/cmssection.agentlist.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                inherit: true,
                    data: function () {
                            return __app.$data;
                        },
                    template: _response,
                    mounted: _mounted,
                    methods: {
                        }
                });
        }

    }

    function _mounted() {

        __app.$popup.loading();
        _allot("cmsagent-controller").fetchList(null, null, _onFinish);

        function _onFinish() {
            __app.$popup.close("loading");
        }

    }

}(window.allot));