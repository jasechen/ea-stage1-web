(function (_allot) {

    "use strict";

    var __query = _allot("query");
    var __claim = _allot("claim");
    var __app = _allot("app");

    var _courseID;

    (function () {

        _allot("cmscoursesingle-directive", _componentDirective);
        _allot("cmscoursesingle-directive-mixin", _definedData());

    }());

    function _componentDirective(_resolve) {

        __app = _allot("app");
        __claim("GET", "/template/cmssection.coursesingle.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                inherit: true,
                    data: function () {
                            return __app.$data;
                        },
                    template: _response,
                    beforeCreate: _beforeCreate,
                    mounted: _mounted,
                    methods: {
                            onModifyClick: _onModifyClick,
                            onModifyCancel: _onModifyCancel,
                            onUpdateClick: _onUpdateClick
                        }
                });
        }

    }

    function _definedData() {

        return {
                data: _procedure
            };

        function _procedure() {
            return {
                    cmsSchoolID: "",
                    courseName: "",
                    courseExcerpt: "",
                    courseTA: "",
                    courseTALocale: ""
                };
        }

    }

    function _beforeCreate() {

        __app.cmsSchoolID = __app.cmsSchoolID || __query.all().school ;
        __app.courseName = "";
        __app.courseExcerpt = "";
        __app.courseTA = "";
        __app.courseTALocale = "";
        _courseID = __app.slug;

    }

    function _mounted() {

        _readSingleDatum();

    }

    function _readSingleDatum() {

        __app.$popup.loading();
        __claim("get", "school/course/{school_course_id}", {
                "{school_course_id}": _courseID
            }, _onSuccess, null, _onFinish);

        function _onSuccess(_response) {
            var _courseDatum = _response.school_course;
            __app.courseName = _courseDatum.name;
            __app.courseExcerpt = _courseDatum.description;
            __app.courseTA = _courseDatum.ta;
            __app.courseTALocale = ( _courseDatum.ta_langs || {} ).tw;
        }

        function _onFinish() {
            __app.$popup.close("loading");
        }

    }

    function _onModifyClick() {

        _changeModifyStat();

    }

    function _onModifyCancel() {

        window.location.reload();

    }

    function _onUpdateClick() {

        __app.$popup.loading();
        __claim("put", "school/course/{school_course_id}", {
                "{school_course_id}": _courseID,
                "name": __app.courseName,
                "description": __app.courseExcerpt,
                "ta": __app.courseTA
            }, _onSuccess, _onError, _onFinish);

        function _onSuccess() {
            __app.$popup.alert("更新成功", _onConfirmSuccess);
        }

        function _onConfirmSuccess() {
            __app.$popup.close("alert");
            window.location.reload();
        }

        function _onError(_response) {
            _response = _response || {} ;
            if ( _response.comment === "updateData empty" ) _onSuccess();
            else __app.$popup.alert("資料有誤");
        }

        function _onFinish() {
            __app.$popup.close("loading");
        }

    }

    function _changeModifyStat() {

        __app.cmsmodify = !(__app.cmsmodify);

    }

}(window.allot));