(function (_allot) {

    "use strict";

    var __depot = _allot("depot");
    var __query = _allot("query");
    var __claim = _allot("claim");
    var __app = _allot("app");
    var __wheel = _allot("wheel");
    var __masonry = _allot("masonry");

    var _queries;
    var _currentList;
    var _lastCardOffset;
    var _pageResponsed;
    var _pageRequest;
    var _apiResponsed;
    var _apiRequest;
    var _filterQueryies;

    (function () {

        _queries = __query.all();
        _allot("fallsview-directive", _componentFallsviewView);
        _allot("fallsview-directive-mixin", _definedData());

    }());

    function _componentFallsviewView(_resolve) {

        __app = _allot("app");
        __claim("GET", "/template/fallsview.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                    data: function () {
                            return _allot("app").$data;
                        },
                    template: _response,
                    beforeCreate: _beforeCreate,
                    mounted: _mounted,
                    updated: _updated,
                    methods: {
                            getFallsviewCardUrl: _getFallsviewCardUrl,
                            getFallsviewCardCoverStyle: _getFallsviewCardCoverStyle,
                            getFallsviewCardAvatar: _getFallsviewCardAvatar
                        },
                    watch: {
                            filterCountry: _onFilterCountryWatched,
                            filterLocation: _onFilterLocationWatched,
                            filterSchooltype: _onFilterSchooltypeWatched,
                            filterProgramtype: _onFilterProgramtypeWatched,
                            filterProgram: _onFilterProgramWatched
                        }
                });
        }

    }

    function _definedData() {

        return {
                data: _procedure
            };

        function _procedure() {
            return {
                    fallsviewLabel: "",
                    cardList: null,
                    filterCountry: _queries.flt_country || "" ,
                    filterLocation: _queries.flt_location || "" ,
                    filterSchooltype: _queries.flt_schooltype || "",
                    filterProgramtype: _queries.flt_programtype || "",
                    filterProgram: _queries.flt_program || ""
                };
        }

    }

    function _beforeCreate() {

        _pageRequest = 0;
        _pageResponsed = 0;

    }

    function _mounted() {

        _defineEvent();
        _readCards();

    }

    function _updated() {

        __masonry.reset();
        _inspectLastCard();

        function _inspectLastCard() {
            var _cardElementList = document.querySelectorAll(__depot.FALLSVIEW_ITEM_SELECTOR);
            var _index = _cardElementList.length;
            var _lastNum = 0;
            var _item;
            while ( _index-- ) {
                _item = _cardElementList[_index];
                if ( _item.offsetTop > _lastNum ) _lastNum = _item.offsetTop;
            }
            _lastCardOffset = _lastNum;
        }

    }

    function _defineEvent() {

        __wheel.add(_onWheel);

        function _onWheel(_offset) {
            if ( _lastCardOffset > _offset.center ) return;
            if ( _pageRequest > _pageResponsed ) return;
            _pageRequest++;
            _readCards();
        }

    }

    function _readCards() {

        _pageRequest = _pageRequest || 1 ;
        switch ( __app.route ) {
            case "search":
                __app.searchStr = __app.slug;
                if ( !(__app.slug) ) return;
                _apiRequest = "search/{term}/";
                __claim("GET", _apiRequest + _pageRequest , {
                        "{term}": __app.slug
                    }, _onSearchSuccess, _onSearchError);
                break;
            case "category":
                _apiRequest = "post/postcategory/slug/{postcategory_slug}/publish/ASC/";
                __claim("GET", _apiRequest + _pageRequest , {
                        "{postcategory_slug}": __app.slug
                    }, _onSuccess);
                break;
            case "agents":
            case "agent":
                _apiRequest = "user/agent/list/enable/ASC/";
                __claim("GET", _apiRequest + _pageRequest , {
                    }, _onSuccess);
                break;
            case "essay":
                _apiRequest = "post/list/publish/ASC/";
                __claim("GET", _apiRequest + _pageRequest , null, _onSuccess);
                break;
            default:
                _readSchoolCards();
        }

        function _onSuccess(_response) {
            _updateCardList(_response);
        }

        function _onSearchSuccess(_response) {
            __app.searchStr = __app.slug;
            _updateCardList(_response);
        }

        function _onSearchError() {
            _updateCardList();
        }

    }

    function _readSchoolCards() {

        var _queries = __query.all();
        var _conditionArray = [];
        if ( _queries.flt_country ) _conditionArray.push(_queries.flt_country);
        if ( _queries.flt_location ) _conditionArray.push(_queries.flt_location);
        if ( _queries.flt_schooltype ) _conditionArray.push(_queries.flt_schooltype);
        if ( _queries.flt_programtype ) _conditionArray.push(_queries.flt_programtype);
        if ( _queries.flt_program ) _conditionArray.push(_queries.flt_program);
        if ( _conditionArray.length ) {
            if ( __query.compare(_filterQueryies) ) return;
            _pageRequest = 1;
            _currentList = [];
            _apiRequest = "school/filter/search";
            _filterQueryies = _queries;
            __claim("POST", _apiRequest, {
                    filter_ids: _conditionArray
                }, _onSuccess);
            window.scrollTo(0, 0);
        } else {
            _apiRequest = "school/list/enable/ASC/";
            __claim("GET", _apiRequest + _pageRequest , null, _onSuccess);
        }

        function _onSuccess(_response) {
            _updateCardList(_response);
        }

    }

    function _getFallsviewCardUrl(_item) {

        switch ( __app.route ) {
            case "category":
            case "essay":
                return "/" + _item.slug ;
            case "agents":
            case "agent":
                return "/agent/" + _item.slug ;
            default:
                return "/school/" + _item.slug ;
        }

    }

    function _getFallsviewCardCoverStyle(_item) {

        var _linkArray = _item.cover_links || _item.profile.avatar_links ;
        var _url = _linkArray.c || _linkArray.o ;
        return "background-image:url(" + __app.publicPath + _url + ")" ;

    }

    function _getFallsviewCardAvatar(_item) {

        return __app.publicPath + ( ( ( _item.owner || {} ).profile || {} ).avatar_links || {} ).o;

    }

    function _onFilterCountryWatched() {

        var _key = "flt_country";
        if ( !(__app.filterCountry) ) __query.deleteItem(_key);
        __app.filterLocation = "";
        __query.addItem(_key, __app.filterCountry);
        _readSchoolCards();

    }

    function _onFilterLocationWatched() {

        var _key = "flt_location";
        if ( !(__app.filterLocation) ) __query.deleteItem(_key);
        __query.addItem(_key, __app.filterLocation);
        _readSchoolCards();

    }

    function _onFilterSchooltypeWatched() {

        var _key = "flt_schooltype";
        if ( !(__app.filterSchooltype) ) __query.deleteItem(_key);
        __query.addItem(_key, __app.filterSchooltype);
        _readSchoolCards();

    }

    function _onFilterProgramtypeWatched() {

        var _key = "flt_programtype";
        if ( !(__app.filterProgramtype) ) __query.deleteItem(_key);
        __app.filterProgram = "";
        __query.addItem(_key, __app.filterProgramtype);
        _readSchoolCards();

    }

    function _onFilterProgramWatched() {

        var _key = "flt_program";
        if ( !(__app.filterProgram) ) __query.deleteItem(_key);
        __query.addItem(_key, __app.filterProgram);
        _readSchoolCards();

    }

    function _updateCardList(_response) {

        if ( _response ) _pageResponsed = _pageRequest;
        _response = _response || {} ;
        var _list = _response.schools || _response.posts ||  _response.users || [] ;
        if ( _currentList && ( _apiRequest === _apiResponsed ) ) _list = _currentList.concat(_list);
        __app.cardList = _list;
        _currentList = __app.cardList;
        if ( __app.cardList.length ) __app.fallsviewLabel = "";
        else {
            document.querySelector(__depot.FALLSVIEW_CONTAINER_SELECTOR).style.height = "auto";
            __app.fallsviewLabel = "無資料";
        }
        _apiResponsed = _apiRequest;

    }

}(window.allot));