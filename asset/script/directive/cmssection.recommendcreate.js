(function (_allot) {

    "use strict";

    var __claim = _allot("claim");
    var __app;

    (function () {

        _allot("cmsrecommendcreate-directive", _componentDirective);

    }());

    function _componentDirective(_resolve) {

        __app = __app || _allot("app") ;
        __claim("GET", "/template/cmssection.recommendcreate.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                    data: function () {
                            return _allot("app").$data;
                        },
                    template: _response,
                    mounted: _mounted,
                    methods: {
                            onCreateClick: _onCreateClick,
                            onImageChange: _onImageChange
                        }
                });
        }

    }

    function _mounted() {

        __app.agentID = _allot("query").getItem("agent");
        _allot("cmsrecommend-controller").initDatum();

    }

    function _onCreateClick() {

        __app.$popup.loading();
        _allot("cmsrecommend-controller").createImage(_onImageSuccess, _onImageError);

        function _onImageSuccess() {
            _allot("cmsrecommend-controller").createSingle(_onSingleSuccess, _onSingleError, _onFinish);
        }

        function _onSingleSuccess() {
            __app.$popup.alert("新增成功", _onConfirmFinish);
        }

        function _onImageError() {
            __app.$popup.close("loading");
            __app.$popup.alert("照片有誤");
        }

        function _onSingleError() {
            __app.$popup.alert("資料有誤");
        }

        function _onFinish() {
            __app.$popup.close("loading");
        }

        function _onConfirmFinish() {
            __app.$popup.close("alert");
            window.location.hash = "#/agent/" + __app.agentID ;
        }

    }

    function _onImageChange(_event) {

        var _file = _event.target.files[0];
        var _fileReader = new FileReader();
        _fileReader.onload = _onFileReadLoaded;
        _fileReader.readAsDataURL(_file);

        function _onFileReadLoaded() {
            _allot("cmsrecommend-controller").updateImageURL(_fileReader.result);
        }

    }

}(window.allot));