(function (_allot) {

    "use strict";

    var __claim = _allot("claim");
    var __app = _allot("app");

    (function () {

        _allot("cmsdormlist-directive", _componentDirective);
        _allot("cmsdormlist-directive-mixin", _definedData());

    }());

    function _componentDirective(_resolve) {

        __app = _allot("app");
        __claim("GET", "/template/cmssection.dormlist.html", null, _onSuccess);

        function _onSuccess(_response) {
            _resolve({
                inherit: true,
                    data: function () {
                            return __app.$data;
                        },
                    template: _response,
                    beforeCreate: _beforeCreate,
                    mounted: _mounted,
                    methods: {
                            confirmExist: _confirmExist
                        }
                });
        }

    }

    function _definedData() {

        return {
                data: _procedure
            };

        function _procedure() {
            return {
                    cmsDormList: []
                };
        }

    }

    function _beforeCreate() {



    }

    function _mounted() {



    }

    function _confirmExist(_typeName) {

        var _index = ( __app.cmsDormList || [] ).length;
        while ( _index-- ) {
            if ( __app.cmsDormList[_index].type === _typeName ) return true;
        }

    }

}(window.allot));