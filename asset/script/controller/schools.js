(function (_allot) {

    "use strict";

    var __query = _allot("query");
    var __claim = _allot("claim");
    var __app = _allot("app");

    var _queries;

    (function () {

        _queries = __query.all();
        _allot("schools-controller", _definedController);
        _allot("schools-controller-mixin", _definedData());

    }());

    function _definedController() {

        __app = _allot("app");
        _readCountryList();

    }

    function _definedData() {

        return {
                data: _procedureData,
                methods: _procedureMethods()
            };

        function _procedureData() {
            return {
                    filterCountryList: [],
                    filterCountry: _queries.flt_country || "",
                    filterLocationList: [],
                    filterLocation: _queries.flt_location || "" ,
                    filterSchooltypeList: [],
                    filterSchooltype: _queries.flt_schooltype || "",
                    filterProgramtypeList: [],
                    filterProgramtype: _queries.flt_programtype || "",
                    filterProgramList: [],
                    filterProgram: _queries.flt_program || "",
                    filterProgramtaList: []
                };
        }

        function _procedureMethods() {
            return {
                    onFilterCountryChange: _onFilterCountryChange,
                    onFilterProgramtypeChange: _onFilterProgramtypeChange
                };
        }

    }

    function _readCountryList() {

        __claim("GET", "/filter/school/type/country", null, _onCountrySuccess);

        function _onCountrySuccess(_response) {
            __app.filterCountryList = _response.filters;
            _readSchooltypeList();
            if ( __app.filterCountry ) _readLocationList();
        }

    }

    function _readLocationList() {

        __claim("GET", "filter/school/type/location/id/" + __app.filterCountry , null, _onLocationSuccess);

        function _onLocationSuccess(_response) {
            __app.filterLocationList = _response.filters;
        }

    }

    function _readSchooltypeList() {

        __claim("GET", "filter/school/type/school-type", null, _onSchooltypeSuccess);

        function _onSchooltypeSuccess(_response) {
            __app.filterSchooltypeList = _response.filters;
            _readProgramtypeList();
        }

    }

    function _readProgramtypeList() {

        __claim("GET", "filter/school/type/program-type", null, _onProgramtypeSuccess);

        function _onProgramtypeSuccess(_response) {
            __app.filterProgramtypeList = _response.filters;
            _readProgramtaList();
            if ( __app.filterProgramtype ) _readProgramList();
        }

    }

    function _readProgramList() {

        __claim("GET", "filter/school/type/program/id/" + __app.filterProgramtype , null, _onProgramSuccess);

        function _onProgramSuccess(_response) {
            __app.filterProgramList = _response.filters;
        }

    }

    function _readProgramtaList() {

        __claim("GET", "filter/school/type/program-ta", null, _onProgramtaSuccess);

        function _onProgramtaSuccess(_response) {
            __app.filterProgramtaList = _response.filters;
        }

    }

    function _onFilterCountryChange() {

        if ( !(__app.filterCountry) ) {
            __app.filterLocationList = [];
            __app.filterLocation = "";
        } else __claim("GET", "filter/school/type/location/id/" + __app.filterCountry , null, _onSuccess);

        function _onSuccess(_response) {
            __app.filterLocationList = _response.filters;
            __app.filterLocation = "";
        }

    }

    function _onFilterProgramtypeChange() {

        if ( !(__app.filterProgramtype) ) {
            __app.filterProgramList = [];
            __app.filterProgram = "";
        } else __claim("GET", "filter/school/type/program/id/" + __app.filterProgramtype , null, _onSuccess);

        function _onSuccess(_response) {
            __app.filterProgramList = _response.filters;
            __app.filterProgram = "";
        }

    }

}(window.allot));