(function (_allot) {

    "use strict";

    var __claim = _allot("claim");
    var __app = _allot("app");

    (function () {

        _allot("cmsskill-controller", _definedController());
        _allot("cmsskill-controller-mixin", _definedData());

    }());

    function _definedController() {

        return {
                initID: _settle(_initID),
                initDatum: _settle(_initDatum),
                fetchList: _settle(_fetchList),
                fetchSingle: _settle(_fetchSingle),
                updateSingle: _settle(_updateSingle),
                createSingle: _settle(_createSingle),
                deleteSingle: _settle(_deleteSingle),
                createImage: _settle(_createImage),
                updateImageURL: _settle(_updateImageURL)
            };

    }

    function _definedData() {

        return {
                data: _procedure
            };

        function _procedure() {
            return {
                    skillID: "",
                    skillList: [],
                    skillDatum: {}
                };
        }

    }

    function _settle(_next) {

        return _procedure;

        function _procedure() {
            __app = __app || _allot("app") ;
            _next.apply(null, arguments);
        }

    }

    function _initID() {

        __app.skillID = __app.slug;

    }

    function _initDatum() {

        __app.skillDatum = {
                id: "",
                title: "",
                excerpt: "",
                imageID: "",
                imageURL: "",
                imageTitle: "",
                imageAlt: ""
            };

    }

    function _fetchList(_callback, _failback, _next) {

        _allot("cmsagent-controller").initID();
        __claim("GET", "user/{id}/agent/intro", {
                "{id}": __app.agentID
            }, _onSuccess, _onError, _onFinish);

        function _onSuccess(_response) {
            __app.skillList = _response.user_intros || [] ;
            if ( _callback ) _callback(_response);
        }

        function _onError(_response) {
            if ( _failback ) _failback(_response);
        }

        function _onFinish() {
            if ( _next ) _next();
        }

    }

    function _fetchSingle(_callback, _failback, _next) {

        __claim("GET", "user/agent/intro/{intro_id}", {
                "{intro_id}": __app.skillID
            }, _onSuccess, _onError, _onFinish);

        function _onSuccess(_response) {
            var _responseSkill = _response.user_intro || {} ;
            __app.skillDatum = {
                    id: _responseSkill.id,
                    title: _responseSkill.title,
                    excerpt: _responseSkill.content,
                    imageID: _responseSkill.image,
                    imageURL: _allot("path").API + ( _responseSkill.image_links.c || _responseSkill.image_links.o ),
                    imageTitle: _responseSkill.image_title,
                    imageAlt: _responseSkill.image_alt
                };
            if ( _callback ) _callback(_response);
        }

        function _onError(_response) {
            if ( _failback ) _failback(_response);
        }

        function _onFinish() {
            if ( _next ) _next();
        }

    }

    function _updateSingle(_callback, _failback, _next) {

        __claim("PUT", "user/agent/intro/{intro_id}", {
                "{intro_id}": __app.skillID,
                "title": __app.skillDatum.title,
                "content": __app.skillDatum.excerpt,
                "image": __app.skillDatum.imageID,
                "image_title": __app.skillDatum.imageTitle,
                "image_alt": __app.skillDatum.imageAlt
            }, _onSuccess, _onError, _onFinish);

        function _onSuccess(_response) {
            if ( _callback ) _callback(_response);
        }

        function _onError(_response) {
            if ( _failback ) _failback(_response);
        }

        function _onFinish() {
            if ( _next ) _next();
        }

    }

    function _createSingle(_callback, _failback, _next) {

        __claim("POST", "user/agent/intro", {
                "user_id": __app.agentID,
                "title": __app.skillDatum.title,
                "content": __app.skillDatum.excerpt,
                "image": __app.skillDatum.imageID,
                "image_title": __app.skillDatum.imageTitle,
                "image_alt": __app.skillDatum.imageAlt
            }, _onSuccess, _onError, _onFinish);

        function _onSuccess(_response) {
            if ( _callback ) _callback(_response);
        }

        function _onError(_response) {
            if ( _failback ) _failback(_response);
        }

        function _onFinish() {
            if ( _next ) _next();
        }

    }

    function _deleteSingle(_callback, _failback, _next) {

        __claim("DELETE", "user/agent/intro/{id}", {
                "{id}": __app.skillID
            }, _onSuccess, _onError, _onFinish);

        function _onSuccess(_response) {
            if ( _callback ) _callback(_response);
        }

        function _onError(_response) {
            if ( _failback ) _failback(_response);
        }

        function _onFinish() {
            if ( _next ) _next();
        }

    }

    function _createImage(_callback, _failback, _next) {

        var _file = document.querySelector("section.form li.picture input[type=file]").files[0];
        __claim("POST", "user/agent/intro/image", {
                "file": _file
            }, _onSuccess, _onError, _onFinish);

        function _onSuccess(_response) {
            __app.skillDatum.imageID = _response.filename;
            if ( _callback ) _callback(_response);
        }

        function _onError(_response) {
            if ( _failback ) _failback(_response);
        }

        function _onFinish() {
            if ( _next ) _next();
        }

    }

    function _updateImageURL(_url) {

        __app.skillDatum.imageURL = _url;

    }

}(window.allot));