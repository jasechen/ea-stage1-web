(function (_allot) {

    "use strict";

    var __claim = _allot("claim");
    var __app = _allot("app");

    (function () {

        _allot("role-controller", _definedController());
        _allot("role-controller-mixin", _definedData());

    }());

    function _definedController() {

        return {
                fetchList: _settle(_fetchList),
                fetchSingle: _settle(_fetchSingle)
            };

    }

    function _definedData() {

        return {
                data: _procedure
            };

        function _procedure() {
            return {
                    roleList: [],
                };
        }

    }

    function _settle(_next) {

        return _procedure;

        function _procedure() {
            __app = __app || _allot("app") ;
            _next.apply(null, arguments);
        }

    }

    function _fetchList(_callback, _failback, _next) {

        __claim("GET", "role/list", {
            }, _onSuccess, _onError, _onFinish);

        function _onSuccess(_response) {
            var _responseRoles = _response.roles || [] ;
            __app.roleList = _responseRoles;
            if ( _callback ) _callback(_responseRoles);
        }

        function _onError(_response) {
            if ( _failback ) _failback(_response);
        }

        function _onFinish() {
            if ( _next ) _next();
        }

    }

    function _fetchSingle(_type, _code, _callback, _failback, _next) {

        __claim("GET", "role/type/{type}/code/{code}", {
                "{type}": _type,
                "{code}": _code
            }, _onSuccess, _onError, _onFinish);

        function _onSuccess(_response) {
            var _responseRole = _response.role || {} ;
            if ( _callback ) _callback(_responseRole);
        }

        function _onError(_response) {
            if ( _failback ) _failback(_response);
        }

        function _onFinish() {
            if ( _next ) _next();
        }

    }

}(window.allot));