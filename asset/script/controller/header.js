(function (_allot) {

    "use strict";

    var __cookie = _allot("cookie");
    var __query = _allot("query");
    var __claim = _allot("claim");
    var __app = _allot("app");

    var _queries;

    (function () {

        _queries = __query.all();
        _allot("header-controller", _definedController);
        _allot("header-controller-mixin", _definedData());

    }());

    function _definedController() {

        __app = _allot("app");

    }

    function _definedData() {

        return {
                data: _procedureData,
                methods: _procedureMethods()
            };

        function _procedureData() {
            return {
                    searchStr: "",
                    isSubscribeVisiable: false,
                    isMobileMenuVisiable: false,
                    subscribleName: "",
                    subscribleEmail: ""
                };
        }

        function _procedureMethods() {
            return {
                    getSubscribeClass: _getSubscribeClass,
                    getMobileMenuClass: _getMobileMenuClass,
                    onHeaderInputKeyup: _onHeaderInputKeyup,
                    onHeaderSearchSubmit: _onHeaderSearchSubmit,
                    onHeaderLoginClick: _onHeaderLoginClick,
                    onHeaderLogoutClick: _onHeaderLogoutClick,
                    onSubscribeVisiable: _onSubscribeVisiable,
                    onSubscribeConfirm: _onSubscribeConfirm,
                    onMobileMenuVisiable: _onMobileMenuVisiable
                };
        }

    }

    function _getSubscribeClass() {

        __app = __app || _allot("app") ;
        return {
                "work": __app.isSubscribeVisiable,
            };

    }
    
    function _getMobileMenuClass() {

        __app = __app || _allot("app") ;
        return {
                "work": __app.isMobileMenuVisiable
            };

    }

    function _onHeaderInputKeyup(_event) {

        if ( _event.keyCode !== 13 || _event.key !== "Enter" ) return;
        _onHeaderSearchSubmit();

    }

    function _onHeaderSearchSubmit() {

        window.location.href = "/search/" + __app.searchStr ;

    }

    function _onHeaderLoginClick() {

        __app.$popup.login();

    }

    function _onHeaderLogoutClick() {

        __app.$popup.confirm("確認要登出？", _onConfirm);

        function _onConfirm() {
            __app.$popup.close("confirm");
            __app.$popup.loading();
            __claim("POST", "logout", null, _onSuccess, _onError, _onFinish);
        }

        function _onSuccess() {
            __cookie.removeItem("LIFETOKEN");
            if ( /\/cms\//.test(window.location.href) ) window.location.href = "/";
            else window.location.reload();
        }

        function _onError() {
            __app.$popup.alert("錯誤發生");
        }

        function _onFinish() {
            __app.$popup.close("loading");
        }

    }

    function _onSubscribeVisiable() {

        __app.isSubscribeVisiable = !(__app.isSubscribeVisiable);

    }
    
    function _onMobileMenuVisiable() {
        __app.isMobileMenuVisiable = !(__app.isMobileMenuVisiable);
    }

    function _onSubscribeConfirm() {

        if ( !(__app.subscribleName) ) return _onError();
        if ( !(__app.subscribleEmail) ) return _onError();
        __claim("POST", "subscribe", {
                "name": __app.subscribleName,
                "email": __app.subscribleEmail
            }, _onSuccess, _onError);

        function _onSuccess() {
            __app.$popup.alert("訂閱成功", _onConfirm);
        }

        function _onError() {
            __app.$popup.alert("資料有誤");
        }

        function _onConfirm() {
            _onSubscribeVisiable();
            __app.$popup.close("alert");
        }

    }

}(window.allot));