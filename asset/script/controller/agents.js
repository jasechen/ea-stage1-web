(function (_allot) {

    "use strict";

    var __cookie = _allot("cookie");
    var __query = _allot("query");
    var __app = _allot("app");

    var _queries;

    (function () {

        _queries = __query.all();
        _allot("agents-controller", _definedController);
        _allot("agents-controller-mixin", _definedData());

    }());

    function _definedController() {

        __app = _allot("app");
        _inspectBrowsingStat();

    }

    function _definedData() {

        return {
                data: _procedureData,
                methods: _procedureMethods()
            };

        function _procedureData() {
            return {

                };
        }

        function _procedureMethods() {
            return {
                };
        }

    }

    function _inspectBrowsingStat() {

        __cookie.removeItem("BROWSING_SCHOOL_STAT");

    }

}(window.allot));