(function (_allot) {

    "use strict";

    var __claim = _allot("claim");
    var __app = _allot("app");

    (function () {

        _allot("cmsagent-controller", _definedController());
        _allot("cmsagent-controller-mixin", _definedData());

    }());

    function _definedController() {

        return {
                initID: _settle(_initID),
                initDatum: _settle(_initDatum),
                fetchList: _settle(_fetchList),
                fetchSingle: _settle(_fetchSingle),
                updateSingle: _settle(_updateSingle),
                createSingle: _settle(_createSingle),
                deleteSingle: _settle(_deleteSingle),
                inspectSingle: _settle(_inspectSingle),
                updateAvatarURL: _settle(_updateAvatarURL)
            };

    }

    function _definedData() {

        return {
                data: _procedure
            };

        function _procedure() {
            return {
                    agentID: "",
                    agentList: [],
                    agentDatum: {},
                    targetCountryList: [],
                    locationDictionary: {},
                    agentDefaultCompanyID: "",
                    agentDefaultRoleID: "",
                    agentBindingArray: [],
                    agentBindingIDArray: []
                };
        }

    }

    function _settle(_next) {

        return _procedure;

        function _procedure() {
            __app = __app || _allot("app") ;
            _next.apply(null, arguments);
        }

    }

    function _initID() {

        __app.agentID = __app.slug;

    }

    function _initDatum() {

        __app.agentDatum = {
                id: "",
                email: "",
                password: "",
                phone: "",
                lastname: "",
                firstname: "",
                nickname: "",
                avatarURL: "",
                avatarTitle: "",
                avatarAlt: "",
                slug: "",
                bio: "",
                experience: "",
                speciality: "",
                seniority: 1,
                targetCountries: [],
                targetCountriesStr: "",
                targetCountryCodes: [],
                locations: [],
                locationsStr: "",
                locationCodes: []
            };

    }

    function _fetchList(_callback, _failback, _next) {

        __claim("GET", "user/agent/list", {
            }, _onSuccess, _onError, _onFinish);

        function _onSuccess(_response) {
            __app.agentList = _response.users || [] ;
            if ( _callback ) _callback(_response);
        }

        function _onError(_response) {
            if ( _failback ) _failback(_response);
        }

        function _onFinish() {
            if ( _next ) _next();
        }

    }

    function _fetchSingle(_callback, _failback, _next) {

        var _responseUser;
        var _responseAgentProfile;
        _readSingle();

        function _readSingle() {
            __claim("GET", "user/{id}", {
                    "{id}": __app.agentID
                }, _onSingleSuccess, _onSingleError);
        }

        function _readAgentProfile() {
            __claim("GET", "user/{id}/agent/profile", {
                    "{id}": __app.agentID
                }, _onAgentProfileSuccess, _onAgentProfileError, _onAgentProfileFinish);
        }

        function _onSingleSuccess(_response) {
            _responseUser = _response.user;
            _readAgentProfile(_responseUser);
        }

        function _onSingleError(_response) {
            if ( _failback ) _failback(_response);
            if ( _next ) _next();
        }

        function _onAgentProfileSuccess(_response) {
            _responseAgentProfile = _response.agent_profile;
            _defineAgentDatum(_responseUser, _responseAgentProfile);
            if ( _callback ) _callback(__app.agentDatum);
        }

        function _onAgentProfileError(_response) {
            if ( _failback ) _failback(_response);
        }

        function _onAgentProfileFinish() {
            if ( _next ) _next();
        }

        function _defineAgentDatum(_responseUser, _responseAgentProfile) {
            var _responseProfile = _responseUser.profile;
            __app.agentDatum = {
                    id: _responseUser.id,
                    email: _responseUser.account,
                    phone: _getInspectPhone(_responseProfile.mobile_phone),
                    firstname: _responseProfile.first_name,
                    lastname: _responseProfile.last_name,
                    nickname: _responseProfile.nickname,
                    avatarURL: _allot("path").API + ( _responseProfile.avatar_links.c || _responseProfile.avatar_links.o ),
                    avatarTitle: _responseUser.avatar_title,
                    avatarAlt: _responseUser.avatar_alt,
                    slug: _responseUser.slug,
                    bio: _responseProfile.bio,
                    experience: _responseAgentProfile.experience,
                    speciality: _responseAgentProfile.expertly_school_types,
                    seniority: _responseAgentProfile.service_years,
                    targetCountries: _responseAgentProfile.familiar_country_langs,
                    targetCountriesStr: _responseAgentProfile.familiar_countries,
                    targetCountryCodes: _getKeyArray(_responseAgentProfile.familiar_country_langs),
                    locations: _responseAgentProfile.service_location_langs,
                    locationsStr: _responseAgentProfile.service_locations,
                    locationCodes: _getKeyArray(_responseAgentProfile.service_location_langs)
                };
        }

        function _getInspectPhone(_phone) {
            if ( _phone.slice(0, 1) === "0" ) return _phone;
            return "0" + _phone ;
        }

        function _getKeyArray(_datum) {
            var _array = [];
            var _key;
            for ( _key in _datum ) _array.push(_key);
            return _array;
        }

    }

    function _updateSingle(_callback, _failback, _next) {

        if ( __app.agentDatum.password ) _updatePassword();
        else _updateProfile();

        function _updatePassword() {
            __claim("PUT", "user/{id}/password", {
                    "{id}": __app.agentID,
                    "password": __app.agentDatum.password,
                    "password_repeat": __app.agentDatum.password
                }, _onPasswordSuccess, _onError);
        }

        function _updateProfile() {
            __claim("PUT", "user/{id}/profile", {
                    "{id}": __app.agentID,
                    "first_name": __app.agentDatum.firstname,
                    "last_name": __app.agentDatum.lastname,
                    "nickname": __app.agentDatum.nickname,
                    "birth": __app.agentDatum.birth,
                    "bio": __app.agentDatum.bio
                }, null, null, _onProfileFinish);
        }

        function _updateAgentProfile() {
            __claim("PUT", "user/{id}/agent/profile", {
                    "{id}": __app.agentID,
                    "experience": __app.agentDatum.experience,
                    "expertly_school_types": __app.agentDatum.speciality,
                    "service_years": __app.agentDatum.seniority,
                    "service_locations": __app.agentDatum.locationCodes.join(),
                    "familiar_countries": __app.agentDatum.targetCountryCodes.join()
                }, null, null, _onAgentProfileFinish);
        }

        function _createAvatar() {
            var _tempFile = document.querySelector(".avatar > input[type=file]").files[0];
            if ( _tempFile ) __claim("post", "user/{id}/avatar", {
                    "{id}": __app.agentID,
                    "file": _tempFile,
                    "status": "enable",
                    "_method": "PUT"
                }, null, null, _onFinish);
            else _onFinish();
        }

        function _onPasswordSuccess() {
            _updateProfile();
        }

        function _onProfileFinish() {
            _updateAgentProfile();
        }

        function _onAgentProfileFinish() {
            _createAvatar();
        }

        function _onError(_response) {
            if ( _failback ) _failback(_response);
            if ( _next ) _next();
        }

        function _onFinish() {
            if ( _callback ) _callback();
            if ( _next ) _next();
        }

    }

    function _createSingle(_callback, _failback, _next) {

        if ( __app.agentDefaultRoleID ) _createUser();
        else _allot("role-controller").fetchSingle("company", "agent", _onAgentRoleSuccess);

        function _createUser() {
            __claim("POST", "user", {
                    "account": __app.agentDatum.email,
                    "password": __app.agentDatum.password,
                    "password_repeat": __app.agentDatum.password,
                    "mobile_phone": _getInspectedPhone(__app.agentDatum.phone),
                    "mobile_country_code" : "886",
                    "first_name": __app.agentDatum.firstname,
                    "last_name": __app.agentDatum.lastname,
                    "avatar_title": __app.agentDatum.avatarTitle,
                    "avatar_alt": __app.agentDatum.avatarAlt,
                    "slug": __app.agentDatum.slug,
                    "excerpt": __app.agentDatum.bio,
                    "og_title": __app.agentDatum.lastname + " "+ __app.agentDatum.firstname ,
                    "og_description": __app.agentDatum.bio,
                    "meta_title": __app.agentDatum.lastname + " "+ __app.agentDatum.firstname ,
                    "meta_description": __app.agentDatum.bio,
                    "status": "enable",
                    "role_ids": [
                            __app.agentDefaultRoleID
                        ]
                }, _onUserSuccess, _onUserError);
        }

        function _updateUserProfile() {
            __claim("put", "user/{id}/profile", {
                    "{id}": __app.agentDatum.id,
                    "nickname": __app.agentDatum.nickname,
                    "bio": __app.agentDatum.bio
                }, _onUserProfileSuccess, _onUserProfileError);
        }

        function _updateCompanyRole() {
            __claim("post", "company/user", {
                    "user_id": __app.agentDatum.id,
                    "company_id": __app.agentDefaultCompanyID,
                    "role_id": __app.agentDefaultRoleID
                }, _onCompanyRoleSuccess, _onCompanyRoleError);
        }

        function _createAgentProfile() {
            __claim("post", "user/agent/profile", {
                    "user_id": __app.agentDatum.id,
                    "experience": __app.agentDatum.experience,
                    "expertly_school_types": __app.agentDatum.speciality,
                    "service_years": __app.agentDatum.seniority,
                    "service_locations": __app.agentDatum.locationCodes.join(),
                    "familiar_countries": __app.agentDatum.targetCountryCodes.join()
                }, null, null, _onAgentProfileFinish);
        }

        function _createAvatar() {
            var _tempFile = document.querySelector(".avatar > input[type=file]").files[0];
            __claim("post", "user/{id}/avatar", {
                    "{id}": __app.agentDatum.id,
                    "file": _tempFile,
                    "status": "enable",
                    "_method": "PUT"
                }, _onAvatarSuccess, null, _onFinish);
        }

        function _onAgentRoleSuccess(_response) {
            __app.agentDefaultRoleID = _response.id;
            _createUser();
        }

        function _onUserSuccess(_response) {
            __app.agentDatum.id = _response.user_id;
            _updateUserProfile();
        }

        function _onUserError(_response) {
            if ( _failback ) _failback(_response);
            if ( _next ) _next();
        }

        function _onUserProfileSuccess() {
            _updateCompanyRole();
        }

        function _onUserProfileError(_response) {
            if ( _failback ) _failback(_response);
            if ( _next ) _next();
        }

        function _onCompanyRoleSuccess() {
            _createAgentProfile();
        }

        function _onCompanyRoleError(_response) {
            if ( _failback ) _failback(_response);
            if ( _next ) _next();
        }

        function _onAgentProfileFinish() {
            _createAvatar();
        }

        function _onAvatarSuccess() {
            if ( _callback ) _callback();
        }

        function _onFinish() {
            if ( _next ) _next();
        }

        function _getInspectedPhone(_phone) {
            return _phone.replace(/^0/, "");
        }

    }

    function _deleteSingle(_callback, _failback, _next) {

        __claim("DELETE", "user/{id}", {
                "{id}": __app.agentID
            }, _onSuccess, _onError, _onFinish);

        function _onSuccess(_response) {
            if ( _callback ) _callback(_response);
        }

        function _onError(_response) {
            if ( _failback ) _failback(_response);
        }

        function _onFinish() {
            if ( _next ) _next();
        }

    }

    function _inspectSingle(_callback, _errorback) {

        if ( !(__app.agentDatum.email) ) return _alertError("信箱");
        if ( !(__app.agentDatum.password) ) return _alertError("密碼");
        if ( !(__app.agentDatum.phone) ) return _alertError("電話");
        if ( !(__app.agentDatum.lastname) ) return _alertError("姓氏");
        if ( !(__app.agentDatum.firstname) ) return _alertError("名字");
        if ( !(__app.agentDatum.avatarURL) ) return _alertError("頭像");
        if ( !(__app.agentDatum.avatarTitle) ) return _alertError("頭像標題");
        if ( !(__app.agentDatum.avatarAlt) ) return _alertError("頭像註明");
        if ( !(__app.agentDatum.slug) ) return _alertError("SLUG");
        if ( !(__app.agentDatum.bio) ) return _alertError("簡介");
        if ( !(__app.agentDatum.experience) ) return _alertError("經驗");
        if ( !(__app.agentDatum.speciality) ) return _alertError("專長");
        if ( !(__app.agentDatum.seniority) ) return _alertError("資歷");
        if ( !(__app.agentDatum.targetCountryCodes.length) ) return _alertError("服務國家");
        if ( !(__app.agentDatum.locationCodes.length) ) return _alertError("地區");
        if ( _callback ) _callback();

        function _alertError(_text) {
            __app.$popup.alert( _text + "有誤" );
            if ( _errorback ) _errorback();
        }

    }

    function _updateAvatarURL(_url) {

        __app.agentDatum.avatarURL = _url;

    }

}(window.allot));