(function (_allot) {

    "use strict";

    var __cookie = _allot("cookie");
    var __query = _allot("query");
    var __claim = _allot("claim");
    var __app = _allot("app");

    var _queries;

    (function () {

        _queries = __query.all();
        _allot("boardview-controller", _definedController);
        _allot("boardview-controller-mixin", _definedData());

    }());

    function _definedController() {

        __app = _allot("app");

    }

    function _definedData() {

        return {
                data: _procedureData,
                methods: _procedureMethods()
            };

        function _procedureData() {
            return {
                    galleryAmount: 0,
                    galleryIndex: 0
                };
        }

        function _procedureMethods() {
            return {
                    onAgentContactClick: _onAgentContactClick,
                    getGalleryContainerStyle: _getGalleryContainerStyle,
                    onGalleryArrowPrevClick: _onGalleryArrowPrevClick,
                    onGalleryArrowNextClick: _onGalleryArrowNextClick
                };
        }

    }

    function _getGalleryContainerStyle() {

        __app = __app || _allot("app") ;
        __app.galleryAmount = document.querySelectorAll("picture.gallery img").length;
        return "transform: translateX(-" + ( ( 100 / __app.galleryAmount ) * __app.galleryIndex ) + "%)";

    }

    function _onGalleryArrowPrevClick() {

        __app.galleryIndex--;

    }

    function _onGalleryArrowNextClick() {

        __app.galleryIndex++;

    }

    function _onAgentContactClick() {

        var _schoolID = __cookie.getItem("BROWSING_SCHOOL_STAT");
        var _agentID = __cookie.getItem("BROWSING_AGENT_STAT");
        if ( !(__app.tokenUser.id) ) __app.$popup.alert("請先登入", _onConfirm);
        else if ( !(_agentID) ) __app.$popup.alert("需求錯誤");
        else _procedureApply();

        function _procedureApply() {
            __app.$popup.loading();
            __claim("POST", "school/apply", {
                    "school_id": _schoolID,
                    "agent_id": _agentID
                }, _onSuccess, _onError, _onFinish);
        }

        function _onConfirm() {
            __app.$popup.close("alert");
            __app.$popup.login();
        }

        function _onSuccess() {
            __app.$popup.alert("已寄送聯絡方式予顧問");
        }

        function _onError(_response) {
            if ( _response.comment === "agent school error" ) {
                __cookie.removeItem("BROWSING_SCHOOL_STAT");
                _procedureApply();
            } else __app.$popup.alert("發生錯誤");
        }

        function _onFinish() {
            __app.$popup.close("loading");
        }

    }

}(window.allot));