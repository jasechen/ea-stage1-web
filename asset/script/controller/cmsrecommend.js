(function (_allot) {

    "use strict";

    var __claim = _allot("claim");
    var __app = _allot("app");

    (function () {

        _allot("cmsrecommend-controller", _definedController());
        _allot("cmsrecommend-controller-mixin", _definedData());

    }());

    function _definedController() {

        return {
                initID: _settle(_initID),
                initDatum: _settle(_initDatum),
                fetchList: _settle(_fetchList),
                fetchSingle: _settle(_fetchSingle),
                updateSingle: _settle(_updateSingle),
                createSingle: _settle(_createSingle),
                deleteSingle: _settle(_deleteSingle),
                createImage: _settle(_createImage),
                updateImageURL: _settle(_updateImageURL)
            };

    }

    function _definedData() {

        return {
                data: _procedure
            };

        function _procedure() {
            return {
                    recommendID: "",
                    recommendList: [],
                    recommendDatum: {}
                };
        }

    }

    function _settle(_next) {

        return _procedure;

        function _procedure() {
            __app = __app || _allot("app") ;
            _next.apply(null, arguments);
        }

    }

    function _initID() {

        __app.recommendID = __app.slug;

    }

    function _initDatum() {

        __app.recommendDatum = {
                id: "",
                title: "",
                excerpt: "",
                imageID: "",
                imageURL: "",
                imageTitle: "",
                imageAlt: ""
            };

    }

    function _fetchList(_callback, _failback, _next) {

        _allot("cmsagent-controller").initID();
        __claim("get", "user/{id}/agent/recommend", {
                "{id}": __app.agentID
            }, _onSuccess, _onError, _onFinish);

        function _onSuccess(_response) {
            __app.recommendList = _response.user_recommends || [] ;
            if ( _callback ) _callback(_response);
        }

        function _onError(_response) {
            if ( _failback ) _failback(_response);
        }

        function _onFinish() {
            if ( _next ) _next();
        }

    }

    function _fetchSingle(_callback, _failback, _next) {

        __claim("GET", "user/agent/recommend/{recommend_id}", {
                "{recommend_id}": __app.recommendID
            }, _onSuccess, _onError, _onFinish);

        function _onSuccess(_response) {
            var _responseSkill = _response.user_recommend || {} ;
            __app.recommendDatum = {
                    id: _responseSkill.id,
                    title: _responseSkill.title,
                    excerpt: _responseSkill.content,
                    imageID: _responseSkill.image,
                    imageURL: _allot("path").API + ( _responseSkill.image_links.c || _responseSkill.image_links.o ),
                    imageTitle: _responseSkill.image_title,
                    imageAlt: _responseSkill.image_alt
                };
            if ( _callback ) _callback(_response);
        }

        function _onError(_response) {
            if ( _failback ) _failback(_response);
        }

        function _onFinish() {
            if ( _next ) _next();
        }

    }

    function _updateSingle(_callback, _failback, _next) {

        __claim("PUT", "user/agent/recommend/{recommend_id}", {
                "{recommend_id}": __app.recommendID,
                "title": __app.recommendDatum.title,
                "content": __app.recommendDatum.excerpt,
                "image": __app.recommendDatum.imageID,
                "image_title": __app.recommendDatum.imageTitle,
                "image_alt": __app.recommendDatum.imageAlt
            }, _onSuccess, _onError, _onFinish);

        function _onSuccess(_response) {
            if ( _callback ) _callback(_response);
        }

        function _onError(_response) {
            if ( _failback ) _failback(_response);
        }

        function _onFinish() {
            if ( _next ) _next();
        }

    }

    function _createSingle(_callback, _failback, _next) {

        __claim("POST", "user/agent/recommend", {
                "user_id": __app.agentID,
                "title": __app.recommendDatum.title,
                "content": __app.recommendDatum.excerpt,
                "image": __app.recommendDatum.imageID,
                "image_title": __app.recommendDatum.imageTitle,
                "image_alt": __app.recommendDatum.imageAlt
            }, _onSuccess, _onError, _onFinish);

        function _onSuccess(_response) {
            if ( _callback ) _callback(_response);
        }

        function _onError(_response) {
            if ( _failback ) _failback(_response);
        }

        function _onFinish() {
            if ( _next ) _next();
        }

    }

    function _deleteSingle(_callback, _failback, _next) {

        __claim("DELETE", "user/agent/recommend/{id}", {
                "{id}": __app.recommendID
            }, _onSuccess, _onError, _onFinish);

        function _onSuccess(_response) {
            if ( _callback ) _callback(_response);
        }

        function _onError(_response) {
            if ( _failback ) _failback(_response);
        }

        function _onFinish() {
            if ( _next ) _next();
        }

    }

    function _createImage(_callback, _failback, _next) {

        var _file = document.querySelector("section.form li.picture input[type=file]").files[0];
        __claim("POST", "user/agent/recommend/image", {
                "file": _file
            }, _onSuccess, _onError, _onFinish);

        function _onSuccess(_response) {
            __app.recommendDatum.imageID = _response.filename;
            if ( _callback ) _callback(_response);
        }

        function _onError(_response) {
            if ( _failback ) _failback(_response);
        }

        function _onFinish() {
            if ( _next ) _next();
        }

    }

    function _updateImageURL(_url) {

        __app.recommendDatum.imageURL = _url;

    }

}(window.allot));