(function (_allot, Vue) {

    "use strict";

    var __cookie = _allot("cookie");
    var __query = _allot("query");
    var __life = _allot("life");
    var __masonry = _allot("masonry");
    var _app;
    var _route;
    var _slug;
    var _queries;

    (function () {

        _queries = __query.all();
        _defineRoute();
        _defineVueApp();

    }());

    function _defineVueApp() {

        _app = new Vue({
                data: {
                        route: _route,
                        slug: _slug,
                        parentSlug: "",
                        publicPath: _allot("path").API,
                        lifetoken: __cookie.LIFETOKEN,
                        sign: !!(__cookie.LIFETOKEN),
                        tokenUser: {
                                id: "",
                                firstName: "",
                                lastName: "",
                                nickname: "",
                                email: "",
                                phone: "",
                                bio: "",
                                status: null,
                                role: ""
                            },
                        richorizonStat: JSON.parse( __cookie.RICHORIZON_STAT || false ),
                        richswitchStat: JSON.parse( __cookie.RICHSWITCH_STAT || false ),
                        cmsprofilemodify: false,
                        cmsSchoolID: ""
                    },
                beforeCreate: _beforeCreated,
                mounted: _mounted,
                methods: {
                        getAppClass: _getAppClass,
                        onRichorizonableHandle: _onRichorizonableHandle,
                        onRichswitchHandle: _onRichswitchHandle
                    },
                components: {
                        "view-popup": _allot("popup-directive"),
                        "view-fallsview": _allot("fallsview-directive"),
                        "view-cmsview": _allot("cmsview-directive"),
                        "view-cmsmenu": _allot("cmsmenu-directive")
                    },
                mixins: [
                        _allot("role-controller-mixin"),
                        _allot("header-controller-mixin"),
                        _allot("boardview-controller-mixin"),
                        _allot("schools-controller-mixin"),
                        _allot("cmsagent-controller-mixin"),
                        _allot("cmsskill-controller-mixin"),
                        _allot("cmsrecommend-controller-mixin"),
                        _allot("popup-directive-mixin"),
                        _allot("fallsview-directive-mixin"),
                        _allot("cmsview-directive-mixin"),
                        _allot("cmsmenu-directive-mixin"),
                        _allot("cmsprofile-directive-mixin"),
                        _allot("cmsschoollist-directive-mixin"),
                        _allot("cmsschoolsingle-directive-mixin"),
                        _allot("cmsschoolcreate-directive-mixin"),
                        _allot("cmscoursesingle-directive-mixin"),
                        _allot("cmscoursecreate-directive-mixin"),
                        _allot("cmsdormlist-directive-mixin"),
                        _allot("cmsdormsingle-directive-mixin"),
                        _allot("cmsdormcreate-directive-mixin"),
                        _allot("cmspicturelist-directive-mixin")
                    ]
            });
        _allot("app", _app);

    }

    function _beforeCreated() {

        _inspcetLife();

    }

    function _mounted() {

        _allot("header-controller")();
        _allot("boardview-controller")();
        if ( _app.route === "schools" ) _allot("schools-controller")();
        if ( _app.route === "agents" ) _allot("agents-controller")();

    }

    function _defineRoute() {

        var _href = window.location.href;
        window.addEventListener("hashchange", _onHashChange);
        if ( /\?$/.test(_href) ) window.location.replace(_href.replace(/\?$/, ""));
        if ( /\/cms\/$/.test(_href) ) window.location.hash = "#/";
        _updateRoute();

        function _onHashChange() {
            _href = window.location.href;
            _updateRoute();
            _inspcetLife();
            window.scrollTo(0, 0);
            _app.cmsmodify = false;
        }

        function _updateRoute() {
            var _hash = window.location.hash.replace("#/", "");
            var _pathname = window.location.pathname.replace(/\/([\w\d\-]+)(\/?[\w\d\-\%]*)*/, "$1");
            if ( _pathname === "/" ) _pathname = "home";
            else if ( /^\//.test(_pathname) ) _pathname = "essay";
            _route = _pathname + _hash ;
            switch ( _route ) {
                case "search":
                case "school":
                case "agent":
                case "category":
                case "essay":
                case "guide":
                    _slug = window.location.pathname.replace(/.*\/([\w\d\-\%]+)$/, "$1");
                    _slug = decodeURI(_slug);
                    break;
                default:
                    _slug = "";
            }
            if ( /^cms/.test(_route) ) {
                _route = _route.replace(/\?.*/, "");
                if ( /^cms\w+\//.test(_route) ) {
                    _slug = _route.replace(/^\w+\/([\w\d]+)(\?.*)?/, "$1");
                    _route = _route.replace(/\/\w+/, "");
                }
            }
            if ( _app ) {
                _app.route = _route;
                _app.slug = _slug;
            }
        }

    }

    function _inspcetLife() {

        __life.session();
        __life.token(_onTokenSuccess, _onTokenError);

        function _onTokenSuccess(_responseUser) {
            _app.tokenUser.id = _responseUser.id;
            _app.tokenUser.firstName = _responseUser.profile.first_name;
            _app.tokenUser.lastName = _responseUser.profile.last_name;
            _app.tokenUser.nickname = _responseUser.profile.nickname;
            _app.tokenUser.email = _responseUser.profile.email;
            _app.tokenUser.phone = _responseUser.profile.phone;
            _app.tokenUser.bio = _responseUser.profile.bio;
            _app.tokenUser.status = _responseUser.status;
            _app.tokenUser.role = _getRole(_responseUser.roles);
            _app.tokenUser.rolelist = _getRoleList(_responseUser.roles);
            _app.tokenUser.emailVerified = !!(_responseUser.email_checked);
            _app.tokenUser.phoneVerified = !!(_responseUser.mobile_checked);
            _app.sign = true;
        }

        function _onTokenError() {
            ( _app || {} ).sign = false;
            if ( /\/cms\//.test(window.location.href) ) {
                if ( _app ) _app.$popup.alert("登入已失效", _onConfirmError);
                else _onConfirmError();
            }
        }

        function _onConfirmError() {
            window.location.replace("/");
        }

        function _getRole(_roles) {
            var _index = _roles.length;
            var _item;
            while ( _index-- ) {
                _item = _roles[_index];
                if ( _item.type !== "general" ) continue;
                if ( _item.code === "member" ) continue;
                return _item.code;
            }
        }

        function _getRoleList(_roles) {
            var _index = _roles.length;
            var _list = [];
            var _item;
            while ( _index-- ) {
                _item = _roles[_index];
                if ( _item.type === "general" ) _list.push(_item.code);
            }
            return _list;
        }

    }

    function _getAppClass() {

        return {
                "sign": _app.sign,
                "richorizon": _app.richorizonStat,
                "richswitch": _app.richswitchStat,
                "unverify": ( _app.tokenUser.status !== "enable" )
            };

    }

    function _onRichorizonableHandle() {

        _app.richorizonStat = !(_app.richorizonStat);
        __cookie.addItem("RICHORIZON_STAT", _app.richorizonStat);
        setTimeout(_onTimeout, 150);
        setTimeout(_onTimeout, 300);

        function _onTimeout() {
            __masonry.update();
        }

    }

    function _onRichswitchHandle() {

        _app.richswitchStat = !(_app.richswitchStat);
        __cookie.addItem("RICHSWITCH_STAT", _app.richswitchStat);
        setTimeout(_onTimeout, 150);
        setTimeout(_onTimeout, 300);

        function _onTimeout() {
            __masonry.update();
        }
    }

}(window.allot, window.Vue));