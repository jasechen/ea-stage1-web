(function (__dir) {

    const _fs = require("fs");

    const _depot = require("./depot");

    (function () {

        module.exports = _procedure;

    }());

    function _procedure(_typeName) {

        const _packerConfigStr = _fs.readFileSync(__dir.asset(_typeName, "packer.json"), _depot.CHAR_SET);
        const _packerConfig = JSON.parse(_packerConfigStr);
        const _files = _packerConfig.index;
        if ( !(_files) ) return _onError();
        var _result = "";
        _files.forEach(_onFilesEach);
        return _result;

        function _onFilesEach(_item) {
            const _data = _fs.readFileSync(__dir.asset(_typeName, _item), _depot.CHAR_SET);
            _result += ( _result.length ? "\n\n" : "" ) + _data ;
        }

        function _onError() {
            console.log("Packer error");
        }

    }

}(global.__dir));