(function () {

    (function () {

        module.exports = {
                HOST: process.env.HOST || "0.0.0.0" ,
                PORT: process.env.PORT || 3001 ,
                CHAR_SET: "utf-8",
                API_INTERFACE_DEV_URL: _getInterfaceURL(),
                JSON_INTERFACE_URL: "/json/",
                appInstance: null,
                cookies: {}
            };

    }());

    function _getInterfaceURL() {

        if ( process.env.EDITION === "DEVELOP" ) return "http://dev.api.v1.english.agency/";
        else if ( process.env.EDITION === "STAGING" ) return "http://api.s1.ea.psycomputing.com/";
        else return "https://apiapp.english.agency/";

    }

}());