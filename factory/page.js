(function () {

    const _app = require("./app");
    const _render = require("./render");
    const _interf = require("./interf");

    const _depot = require("./depot");

    var _resCookie;

    (function () {

        module.exports = _procedure;

    }());

    function _procedure(_url, _callback, _resCookieInstance) {

        const _routeName = _parseRoute(_url);
        _resCookie = _resCookieInstance;
        if ( _routeName ) _readContextDatum(_routeName, _url, _doRender);
        else _onComplete();

        function _doRender(_context) {
            const _appInstance = _app.create(_context, _routeName);
            _context = _setContext(_context);
            _render(_appInstance, _context, _onComplete);
        }

        function _onComplete(_result) {
            if ( _callback ) _callback(_result);
        }

        function _setContext(_context) {
            _context.route = _routeName;
            _context.title = _context.title || "English.Agency 英文職涯情報站－學英文、找工作、拚未來";
            _context.titleMETA = _context.titleMETA || _context.title ;
            _context.titleOG = _context.titleOG || _context.title ;
            _context.exerpt = _context.exerpt || "English.Agency 英文職涯情報站－學英文、找工作、拚未來";
            _context.exerptMETA = _context.exerptMETA || _context.exerpt ;
            _context.exerptOG = _context.exerptOG || _context.exerpt ;
            return _context;
        }

    }

    function _parseRoute(_url) {

        if ( _url === "/" ) return "home";
        if ( /^\/search\//.test(_url) ) return "home";
        if ( /^\/category\//.test(_url) ) return "category";
        if ( /^\/schools(\?[\w\d=_\-%]|$)/.test(_url) ) return "schools";
        if ( /^\/school\//.test(_url) ) return "school";
        if ( /^\/agents(\?[\w\d=_\-%]|$)/.test(_url) ) return "agents";
        if ( /^\/agent\//.test(_url) ) return "agent";
        if ( /^\/(about|join)\-us(\/[\w\d\-\%]+)?$/.test(_url) ) return "guide";
        if ( /^\/cms\//.test(_url) ) return "cms";
        if ( /^\/[\w\d\-\%]+$/.test(_url) ) return "essay";

    }

    function _readContextDatum(_routeName, _url, _callback) {

        if ( _routeName === "home" ) _readHomeContextDatum(_callback);
        else if ( _routeName === "search" ) _readHomeContextDatum(_callback);
        else if ( _routeName === "category" ) _readCategoryContextDatum(_url, _callback);
        else if ( _routeName === "schools" ) _readSchoolsContextDatum(_url, _callback);
        else if ( _routeName === "school" ) _readSchoolContextDatum(_url, _callback);
        else if ( _routeName === "agents" ) _readAgentsContextDatum(_url, _callback);
        else if ( _routeName === "agent" ) _readAgentContextDatum(_url, _callback);
        else if ( _routeName === "guide" ) _readGuideContextDatum(_url, _callback);
        else if ( _routeName === "cms" ) _readCMSContextDatum(_callback);
        else if ( _routeName === "essay" ) _readEssayContextDatum(_url, _callback);

    }

    function _readHomeContextDatum(_callback) {

        if ( _callback ) _callback({});

    }

    function _readCategoryContextDatum(_url, _callback) {

        const _slug = _url.replace("/category/", "");
        var _categoryDatum;
        _interf.get("postcategory/slug/{slug}", {
                "{slug}": _slug
            }, _onCategory);

        function _onCategory(_response) {
            _categoryDatum = _response.postcategory;
            _interf.get("post/recommended/postcategory/{postcategory_id}", {
                    "{postcategory_id}": _categoryDatum.id
                }, _onRecommendsSuccess);
        }

        function _onRecommendsSuccess(_response) {
            const _recommendList = _response.posts || [] ;
            if ( _callback ) _callback({
                    slug: _categoryDatum.slug,
                    title: _categoryDatum.name,
                    exerpt: _categoryDatum.excerpt,
                    content: _categoryDatum.excerpt,
                    cover: _getCoverPath(_categoryDatum),
                    recommends: _recommendList
                });
        }

    }

    function _readSchoolsContextDatum(_url, _callback) {

        if ( _callback ) _callback({
                title: "學校群",
                exerpt: "學校群"
            });

    }

    function _readSchoolContextDatum(_url, _callback) {

        const _slug = _url.replace("/school/", "");
        var _schoolDatum;
        var _courseList;
        var _agentList;
        var _datum = {};
        _interf.get("school/slug/{slug}", {
                "{slug}": _slug
            }, _onSchoolSuccess, _consign);

        function _onSchoolSuccess(_response) {
            _schoolDatum = _response.school;
            _interf.get("school/{id}/course", {
                    "{id}": _schoolDatum.id
                }, _onCourseSuccess, _consign);
        }

        function _onCourseSuccess(_response) {
            _courseList = _response.courses;
            _interf.get("school/{id}/user/type/company", {
                    "{id}": _schoolDatum.id
                }, _onAgentSuccess, null, _consign);
        }

        function _onAgentSuccess(_response) {
            _agentList = _response.school_users;
        }

        function _getGalleryList(_schoolDatum) {
            var _list = [];
            var _imageList = Array.prototype.slice.apply(_schoolDatum.image_links);
            var _item;
            while ( _imageList.length ) {
                _item = _imageList.shift();
                if ( !(_item) ) continue;
                _list.push( _depot.API_INTERFACE_DEV_URL + _item.o );
            }
            return _list;
        }

        function _consign() {
            _resCookie("BROWSING_SCHOOL_STAT", _schoolDatum.id);
            _datum = {
                    apptype: "single",
                    id: _schoolDatum.id,
                    slug: _schoolDatum.slug,
                    title: _schoolDatum.name,
                    exerpt: _schoolDatum.excerpt,
                    date: _schoolDatum.updated_at,
                    cover: _getCoverPath(_schoolDatum),
                    galleryList: _getGalleryList(_schoolDatum),
                    content: _schoolDatum.description,
                    facility: _schoolDatum.facility,
                    locations: _schoolDatum.location_langs,
                    spheres: _schoolDatum.type_langs,
                    dorms: _schoolDatum.dorms,
                    dorm: _schoolDatum.dorms[0],
                    courseList: _courseList,
                    agentList: _agentList
                };
            if ( _callback ) _callback(_datum);
        }

    }

    function _readAgentsContextDatum(_url, _callback) {

        if ( _callback ) _callback({
                title: "找顧問",
                exerpt: "找顧問"
            });

    }

    function _readAgentContextDatum(_url, _callback) {

        const _slug = _url.replace("/agent/", "");
        var _userDatum;
        var _profileDatum;
        var _skillList;
        var _recommendList;
        var _cv = {};
        _interf.get("user/slug/{slug}", {
                "{slug}": _slug
            }, _onAgentSuccess, _consign);

        function _onAgentSuccess(_response) {
            _userDatum = _response.user;
            _profileDatum = _userDatum.profile || {} ;
            _interf.get("user/{id}/agent/profile", {
                    "{id}": _userDatum.id
                }, _onAgentCVSuccess, _consign);
        }

        function _onAgentCVSuccess(_response) {
            _cv = _response.agent_profile;
            _interf.get("user/{id}/agent/intro", {
                    "{id}": _userDatum.id
                }, _onSkillSuccess, _consign);
        }

        function _onSkillSuccess(_response) {
            _skillList = _response.user_intros;
            _interf.get("user/{id}/agent/recommend", {
                    "{id}": _userDatum.id
                }, _onRecommendSuccess, _consign);
        }

        function _onRecommendSuccess(_response) {
            _recommendList = _response.user_recommends;
            _consign();
        }

        function _consign() {
            _resCookie("BROWSING_AGENT_STAT", _userDatum.id);
            if ( _callback ) _callback({
                    apptype: "single",
                    slug: _slug,
                    title: _profileDatum.nickname || ( _profileDatum.last_name + _profileDatum.first_name ),
                    exerpt: _profileDatum.bio,
                    experience: _cv.experience,
                    seniority: _cv.service_years,
                    avatar: _getAvatarPath(_profileDatum),
                    sphere: _cv.expertly_school_types,
                    targetCountry: _cv.familiar_countries,
                    targetCountryDictionary: _cv.familiar_country_langs,
                    location: _cv.service_locations,
                    locationDictionary: _cv.service_location_langs,
                    skillList: _skillList,
                    recommendList: _recommendList
                });
        }

    }

    function _readGuideContextDatum(_url, _callback) {

        const _slug = _url.replace(/^\/((about|join)\-us\/)?/, "");
        if ( _callback ) _callback({
                apptype: "single",
                slug: _slug,
                title: decodeURI(_slug),
                exerpt: ""
            });

    }

    function _readCMSContextDatum(_callback) {

        if ( _callback ) _callback({
                apptype: "cms",
                title: "CMS of English.Agency",
                exerpt: "CMS of English.Agency"
            });

    }

    function _readEssayContextDatum(_url, _callback) {

        const _slug = _url.replace("/", "");
        _interf.get("post/slug/{slug}", {
                "{slug}": _slug
            }, _onSuccess);

        function _onSuccess(_response) {
            const _essayDatum = _response.post || {} ;
            _essayDatum.category = _essayDatum.postcategory || {} ;
            _essayDatum.author = _essayDatum.owner || {} ;
            _essayDatum.author.profile = _essayDatum.author.profile || {} ;
            if ( _callback ) _callback({
                    apptype: "single",
                    slug: _essayDatum.slug,
                    title: _essayDatum.title,
                    exerpt: _essayDatum.excerpt,
                    date: _essayDatum.updated_at,
                    cover: _getCoverPath(_essayDatum),
                    content: _essayDatum.content,
                    categorySlug: _essayDatum.category.slug,
                    categoryTitle: _essayDatum.category.name,
                    authorName: _essayDatum.author.profile.nickname,
                    authorAvatar: _getAvatarPath(_essayDatum.author.profile),
                    authorDescription: _essayDatum.author.profile.bio
                });
        }

    }

    function _getCoverPath(_datum) {

        return _depot.API_INTERFACE_DEV_URL + ( _datum.cover_links || {} ).o ;

    }

    function _getAvatarPath(_datum) {

        return _depot.API_INTERFACE_DEV_URL + ( _datum.avatar_links || {} ).o ;

    }

}());