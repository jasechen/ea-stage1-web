(function () {

    const _fs = require("fs");
    const _request = require("request");

    const _depot = require("./depot");

    (function () {

        module.exports = {
                get: _sendAPIInterface,
                json: _sendJSONInterface,
                fileSync: _sendFileSyncInterface
            };

    }());

    function _sendAPIInterface(_interfaceURL, _data, _callback, _errorback, _finishback) {

        _interfaceURL = _getParamURL();
        _request({
                url: _depot.API_INTERFACE_DEV_URL + _interfaceURL,
                headers: {
                    }
            }, _onComplete);

        function _getParamURL() {
            const _paramArray = _interfaceURL.match(/\{[\w\d\-\_]+\}/g);
            var _item;
            var _value;
            while ( _paramArray.length ) {
                _item = _paramArray.shift();
                _value = _data[_item];
                if ( _value ) _interfaceURL = _interfaceURL.replace(_item, _value);
            }
            return _interfaceURL;
        }

        function _onComplete(_error, _xhr, _response) {

            try {
                _response = _getParsed(_response);
                if ( !(_error) && /20\d/.test(_xhr.statusCode) ) {
                    _response = _response.data || _response ;
                    if ( _callback ) _callback(_response, _xhr);
                } else {
                    _onError.apply(null, arguments);
                }
                if ( _finishback ) _finishback(_xhr);
            } catch (_catchError) {
                _onError(_error, _xhr, _response, _catchError);
            }
        }

        function _onError(_error, _xhr, _response, _catchError) {
            _error =  _error || ( _response.comment || _response ) ;
            if ( _errorback ) _errorback(_error);
            console.group(_interfaceURL, `[${( _xhr || {} ).statusCode}]`);
            console.error(`------\n${_error}\n`);
            if ( _catchError ) console.error(_catchError);
            console.error(`------`);
            console.groupEnd();
        }

    }

    function _sendJSONInterface(_interfaceURL, _callback, _errorback, _finishback) {

        _request( _depot.JSON_INTERFACE_URL + _interfaceURL + ".json" , _onComplete);

        function _onComplete(_error, _xhr, _response) {
            _response = _getParsed(_response);
            if ( !(_error) && /20\d/.test(_xhr.statusCode) ) {
                _response = _response.data || _response ;
                if ( _callback ) _callback(_response, _xhr);
            } else if ( _errorback ) _errorback( _error || ( _response.comment || _response ) );
            if ( _finishback ) _finishback(_xhr);
        }

    }

    function _sendFileSyncInterface(_interfaceURL) {

        var _response = _fs.readFileSync(_interfaceURL, _depot.CHAR_SET);
        return _getParsed(_response);

    }

    function _getParsed(_response) {

        return JSON.parse( _response || null ) || {} ;

    }

}());