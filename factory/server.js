(function (__dir) {

    const _fs = require("fs");
    const _express = require("express");
    const _cookieParser = require("cookie-parser");

    const _depot = require("./depot");
    const _packer = require("./packer");
    const _page = require("./page");

    var _server;

    (function () {

        module.exports = {
                create: _createServer
            };

    }());

    function _createServer() {

        _initialServer();
        _setFolder();
        _setRouter();
        _bootServer();
        _log();

    }

    function _initialServer() {

        _server = _express();
        _server.use(_cookieParser());

    }

    function _setFolder() {

        _server.use("/image", _express.static(__dir.asset("image")));
        _server.use("/template", _express.static(__dir.join("template")));
        _server.use("/event", _express.static(__dir.asset("event")));
        _server.use("/json", _express.static(__dir.join("json")));
        _server.use("/font", _express.static(__dir.asset("font")));

    }

    function _setRouter() {

        _server.get("/script", _doPacker("script", "text/javascript"));
        _server.get("/style", _doPacker("style", "text/css"));
        _server.get("*.*", _doBreak);
        _server.get("*", _doAll);

    }

    function _bootServer() {

        if ( process.env.EDITION === "RELEASE" ) {
            require("http").createServer(_redirectHTTP).listen(80);
            require("https").createServer({
                    key: _fs.readFileSync("/etc/ssl/server.key"),
                    cert: _fs.readFileSync("/etc/ssl/server.crt"),
                    ca: [
                            _fs.readFileSync("/etc/ssl/gd_bundle-g2-g1.crt")
                        ]
                }, _server).listen(_depot.PORT);
        } else _server.listen(_depot.PORT, _depot.HOST);

        function _redirectHTTP(_req, _res) {
            _res.writeHead(301, {
                    "Location": `https://${_req.headers.host}${_req.url}`
                });
            _res.end();
        }

    }

    function _log() {

        console.log("————————————————————————————————————————————————");
        console.log(`Server is running ${process.env.EDITION} on ${_depot.HOST}:${_depot.PORT}`);
        console.log(`${(new Date()).toLocaleString()}`);
        console.log("————————————————————————————————————————————————");

    }

    function _doPacker(_packerName, _contentType) {

        return _procedure;

        function _procedure(_req, _res) {
            const _result = _packer(_packerName);
            if ( _contentType ) _res.set("content-type", _contentType);
            _res.end(_result);
        }

    }

    function _doBreak(_req, _res) {

        _res.status(404).end();

    }

    function _doAll(_req, _res) {

        _depot.cookies = _req.cookies;
        _page(_req.originalUrl, _onComplete, _resCookie);

        function _onComplete(_result) {
            if ( _result ) _res.end(_result);
        }

        function _resCookie(_key, _value) {
            if ( !(_key) ) return;
            if ( !(_value) ) return;
            _res.cookie(_key, _value);
        }

    }

}(global.__dir));