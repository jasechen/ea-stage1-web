(function (__dir) {

    const _fs = require("fs");
    const Vue = require("vue");

    const _depot = require("./depot");
    const _menuDirective = require(__dir("directive", "menu"));

    var _datumContext;
    var _route;
    var _appInstance;

    (function () {

        module.exports = {
                create: _doCreate
            };

    }());

    function _doCreate(_datum, _routeName) {

        _datumContext = _datum;
        _route = _routeName;
        _depot.appInstance = _appInstance = new Vue({
                data: {
                        title: _datum.title,
                        richorizonStat: true,
                        publicPath: _depot.API_INTERFACE_DEV_URL,
                        hypercategoryList: []
                    },
                template: _getTemplate(_route, _datum.apptype),
                components: {
                        "view-header": _componentRegular(_datum, "header"),
                        "view-standview": _componentViewStandview(_datum),
                        "view-boardview": _componentViewBoardview(_datum),
                        "view-menu": _menuDirective(_datum),
                        "view-footer": _componentRegular(_datum, "footer")
                    },
                methods: {
                        getAppTemplateClass: _getAppTemplateClass,
                        confirmObjectEmpty: _confirmObjectEmpty
                    }
            });
        return _appInstance;

    }

    function _getTemplate(_route, _apptype) {

        let _templateName = "app" + ( _apptype ? `.${_apptype}` : "" ) ;
        return _fs.readFileSync(`./template/${_templateName}.html`, _depot.CHAR_SET);

    }

    function _componentRegular(_datum, _templateName) {

        return _procedure;

        function _procedure(_resolve) {
            const _context = {
                    data: function () {
                            return _datum;
                        },
                    template: _fs.readFileSync(`./template/${_templateName}.html`, _depot.CHAR_SET)
                };
            _resolve(_context);
        }

    }

    function _componentViewStandview(_datum) {
        return _procedure;

        function _procedure(_resolve) {
            const _context = {
                    data: function () {
                            _assign(_appInstance.$data, _datum);
                            return _appInstance.$data;
                        },
                    template: _fs.readFileSync("./template/standview.html", _depot.CHAR_SET),
                    components: {
                            "view-standview-article": _componentViewArticle(_datum)
                        }
                };
            _resolve(_context);
        }

    }

    function _componentViewBoardview(_datum) {

        return _procedure;

        function _procedure(_resolve) {
            const _context = {
                    data: function () {
                            return _datum;
                        },
                    template: _fs.readFileSync("./template/boardview.html", _depot.CHAR_SET),
                    components: {
                            "view-boardview-article": _componentViewArticle(_datum),
                        }
                };
            _resolve(_context);
        }

    }

    function _componentViewArticle(_datum) {

        return _procedure;

        function _procedure(_resolve) {
            const _context = {
                    data: function () {
                            _assign(_appInstance.$data, _datum);
                            return _appInstance.$data;
                        },
                    template: _fs.readFileSync(`./template/article.${_datum.route}.html`, _depot.CHAR_SET),
                    methods: {
                            confirmObjectEmpty: _confirmObjectEmpty
                        }
                };
            _resolve(_context);
        }

    }

    function _getAppTemplateClass() {

        const _classDictionary = {};
        _classDictionary[_route] = !!(_route);
        return _classDictionary;

    }

    function _confirmObjectEmpty(_object) {

        var _key;
        for ( _key in _object ) return false;
        return true;

    }

    function _assign(_targetObject, _extraObject) {

        var _key;
        for ( _key in _extraObject ) {
            _targetObject[_key] = _extraObject[_key];
        }

    }

}(global.__dir));