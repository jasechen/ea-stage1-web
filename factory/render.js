(function () {

    const _fs = require("fs");
    const _vuessr = require("vue-server-renderer");

    const _depot = require("./depot");

    var renderer;

    (function () {

        module.exports = _procedure;

    }());

    function _procedure(_app, _context, _callback) {

        renderer = _vuessr.createRenderer({
                template: _fs.readFileSync(`./template/hypertext.html`, _depot.CHAR_SET)
            });
        _context.EXTRA_TAGMNG1_SCRIPT = `<!-- Google Tag Manager --><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-538MSD2');</script><!-- End Google Tag Manager -->`;
        _context.EXTRA_TAGMNG2_SCRIPT = `<!-- Google Tag Manager (noscript) --><noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-538MSD2" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><!-- End Google Tag Manager (noscript) -->`;
        _context.EXTRA_AGENTFORM_SCRIPT = `<script>(function(d, s, id) {window.FormSys = window.FormSys || {};var js, formJs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src = "//form.goeducation.tw/formloader.js";formJs.parentNode.insertBefore(js, formJs);}(document, "script", "form-sys-script"));</script>`;
        renderer.renderToString(_app, _context, _onComplete);

        function _onComplete(_error, _html) {
            if ( _error ) console.error(_error);
            if ( _callback ) _callback(_html);
        }

    }

}());