(function () {

    const _path = require("path");
    var _rootPath = "";

    (function () {

        _rootPath = _path.dirname( require.main.filename || process.mainModule.filename );
        _procedure.root = _rootPath;
        _procedure.join = _doJoin;
        _procedure.asset = _doAsset;
        global.__dir = _procedure;
        module.exports = {
                root: _rootPath,
                join: _doJoin,
                asset: _doAsset
            };

    }());

    function _procedure() {

        return _doJoin.apply(null, arguments);

    }

    function _doJoin() {

        const _argu = Array.prototype.slice.apply(arguments);
        _argu.unshift(_rootPath);
        return _path.join.apply(null, _argu);

    }

    function _doAsset() {

        const _argu = Array.prototype.slice.apply(arguments);
        _argu.unshift("asset");
        return _doJoin.apply(null, _argu);

    }

}());